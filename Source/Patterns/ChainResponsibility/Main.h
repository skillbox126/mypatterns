//// Здесь создается экземпляр базового обработчика (BaseHandler), экземпляры классов SpecificHandler и AnotherSpecificHandler,
// после чего устанавливаются связи между обработчиками с помощью метода SetNextHandler(). Затем вызывается метод HandleRequest()
// базового обработчика, который запускает обработку запроса цепочкой обработчиков.

#pragma once

#include "CoreMinimal.h"
#include "SpecificHandler.h"
#include "AnotherSpecificHandler.h"

inline int main()
{
	BaseHandler* baseHandler = new BaseHandler();
	SpecificHandler* specificHandler = new SpecificHandler();
	AnotherSpecificHandler* anotherSpecificHandler = new AnotherSpecificHandler();

	// Устанавливаем связи между обработчиками
	baseHandler->SetNextHandler(specificHandler);
	specificHandler->SetNextHandler(anotherSpecificHandler);

	// Выполняем запрос, который будет обработан цепочкой обработчиков
	baseHandler->HandleRequest(/* Параметры запроса #1# */);

	delete baseHandler;
	delete specificHandler;
	delete anotherSpecificHandler;

	return 0;
}