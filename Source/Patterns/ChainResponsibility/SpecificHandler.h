// SpecificHandler реализует обработку запроса.
// Если текущий обработчик не может обработать запрос, он передает запрос следующему обработчику в цепочке.

#pragma once

#include "CoreMinimal.h"
#include "BaseHandle.h"

/**
 * 
 */
class PATTERNS_API SpecificHandler : public BaseHandler
{
public:
	void HandleRequest(/* Параметры запроса */) override;
};

