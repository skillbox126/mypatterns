//Теперь класс AnotherSpecificHandler аналогичен классу SpecificHandler, но обладает своей уникальной логикой обработки запросов.
//Оба класса наследуются от BaseHandler и реализуют метод HandleRequest(/ Параметры запроса /) в соответствии с требуемой логикой.


#include "AnotherSpecificHandler.h"

void AnotherSpecificHandler::HandleRequest(/* Параметры запроса */)
{
  bool canHandle = false; // Например, проверка, может ли данный обработчик обработать запрос

  if(canHandle)
  {
    // Обработка запроса в AnotherSpecificHandler
  }
  else if(NextHandler)
  {
    NextHandler->HandleRequest(/* Параметры запроса */); // Передача запроса следующему обработчику в цепочке
  }
}
