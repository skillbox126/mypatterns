//создадим еще один класс обработчика под названием AnotherSpecificHandler,
//который будет аналогичен классу SpecificHandler, но с собственной логикой обработки запросов.

#pragma once

#include "CoreMinimal.h"
#include "BaseHandle.h"
/**
 * 
 */
class PATTERNS_API AnotherSpecificHandler : public BaseHandler
{
	public:
	void HandleRequest(/* Параметры запроса */) override;
};

