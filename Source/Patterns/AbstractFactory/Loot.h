// В этой структуре набор неких свойств без специфической логиги.
//Этот ряд свойств мы хотим сгруппировать. Можем поместить эту структуру в датасет или дататейбл.
//Задавать эти свойства в этом датасете. И динамически получать дефолтные свойства, который будем передавать наследникам.

#pragma once

// #include "CoreMinimal.h"

/** @breaf A base armor info to use in the asset table, for example
 * Базовая информация о броне для использования в таблице активов, например
 */

USTRUCT(BlueprintType)
struct FArmorInfo {
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category= LootStats)
	float Durability{ 0.f }; //Долговечность или выносливость
};

/** @breaf A base weapon info to use in the asset table, for example
* Базовая информация об оружии для использования в таблице активов, например
*/
	
USTRUCT(BlueprintType)
struct FWeaponInfo {
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category= LootStats)
	float Damage{ 0.f };

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category= LootStats)
	float Durability{ 0.f };
};
