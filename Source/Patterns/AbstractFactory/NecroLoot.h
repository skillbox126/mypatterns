// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Loot.h"
#include "Item.h"
#include "NecroLoot.generated.h"

/** @breaf An element of necromant armor. - Элемент брони некроманта.
 */

UCLASS()
// class PATTERNS_API ANecroArmor : public AItem, private FArmorInfo
class PATTERNS_API ANecroArmor : public AItem, public FArmorInfo

{
	GENERATED_BODY()
	
public:	
	virtual void StoreItem_BP() override;
	virtual void UseItem_BP() override;
	virtual void SetupWithInfo(void* InfoPtr) override;
};

inline void ANecroArmor::SetupWithInfo(void* InfoPtr) {
	auto Armor = dynamic_cast<FArmorInfo*>(InfoPtr);
	if(Armor)
		Durability = Armor->Durability;
}

/** @breaf An element of necromant weapon. - Элемент некромантской брони
 */
UCLASS()
class PATTERNS_API ANecroWeapon : public AItem, public FWeaponInfo
{
	GENERATED_BODY()
	virtual void StoreItem_BP() override;
	virtual void UseItem_BP() override;
	virtual void SetupWithInfo(void* InfoPtr) override;
};

inline void ANecroWeapon::SetupWithInfo(void* InfoPtr) {
	auto Weapon = dynamic_cast<FWeaponInfo*>(InfoPtr);
	if(Weapon) {
		Damage = Weapon->Damage;
		Durability = Weapon->Durability;
	}
		
}