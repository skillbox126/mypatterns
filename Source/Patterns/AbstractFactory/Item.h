//Описывает поведение добавления предмета в инвентарь, использования его.
//Именно наследники типа AItem будут определять тип, кот-й они желают получить  - кастовать к нему и получать все необходимые данные.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Item.generated.h"

class USkeletalMesh;

/** @breaf An Item that can be stored in the inventory or used in some way
 * Item - Предмет, который можно хранить в инвентаре или каким-либо образом использовать
*/

UCLASS()
class PATTERNS_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	/** @breaf Puts this Item to our inventory - Помещаем этот предмет в наш инвентарь
	*/
	UFUNCTION(BlueprintCallable)
	virtual void StoreItem_BP() PURE_VIRTUAL(AItem::StoreItem_BP, );

	/** @breaf Uses this item for something - Использует этот предмет для чего-то
	*/
	UFUNCTION(BlueprintCallable)
	virtual void UseItem_BP() PURE_VIRTUAL(AItem::UseItem_BP, );

	/** @breaf Each descendent knows, what info it awaits and how to use it - Каждый потомок знает, какая информация его ждет и как ее использовать
	 * Сохраняем инфу об Item
	*/
	virtual void SetupWithInfo(void* InfoPtr = nullptr);

	// virtual void SetupWithInfo(void* InfoPtr = nullptr) = 0;
	//void* InfoPtr - указатель на объект неизвестного типа. Выражение "= 0" в объявлении виртуальной функции означает,
	//что данная функция является чисто виртуальной функцией, но!

	//Ошибка "UCLASS is not allowed to have pure virtual member functions" возникает при попытке объявить чисто виртуальную функцию в классе,
	//который помечен макросом UCLASS.

	// Чтобы исправить ошибку, вы можете удалить макрос UCLASS из вашего класса, если он не требуется для каких-то специфических Unreal Engine функций.
	// Если же макрос UCLASS необходим, вам придется изменить метод SetupWithInfo - либо предоставить ему реализацию по умолчанию,
	// либо перенести его в другой класс, который не помечен макросом UCLASS.


private:
	
	/** @breaf Mesh of an actual item descendent - Мэш (сетка) фактического потомка элемента
	 * Т.к. этот класс отвечает за внешний вид Item  - он содержит указательна Меш, который будет исп-ся при спауне.
	*/
	//В уроке :
	// UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category= Meshes)
	//в приватной зоне не нужно указывать BlueprintReadOnly, так как за ним идет разрешающий редактирование, что противоречит друг другу
	UPROPERTY( EditDefaultsOnly, Category= Meshes)
	USkeletalMesh* ItemMesh{ nullptr };

	
	
};
