//Эльфийский лут - Наследуется от AItem + приватное! НИФИГа -публичноЕ! наследование
//от FArmorInfo - он их содержит (Durability) но никому не говорит,
//что у него есть эти поля. Об этом никому и не надо знать. Это его личная Инфа.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Loot.h"
#include "ElvenLoot.generated.h"

/** @breaf An element of elven armor. - Элемент эльфийской брони
 */

UCLASS()
class PATTERNS_API AElvenArmor : public AItem, public FArmorInfo
{
	GENERATED_BODY()
	
public:	
	virtual void StoreItem_BP() override;
	virtual void UseItem_BP() override;
	//Этот метод получает неопределенный указатель на структуру, которую мы должны будем скопировать к себе.
	virtual void SetupWithInfo(void* InfoPtr) override;
};

inline void AElvenArmor::SetupWithInfo(void* InfoPtr) {
	//Кастует к классу с которым он работает. Это FArmorInfo - специфический класс из Loot.h
	auto Armor = dynamic_cast<FArmorInfo*>(InfoPtr);
	if(Armor)
		//получаем нужные поля и копируем их к себе
		Durability = Armor->Durability;
}

/** @breaf An element of elven weapon. - Элемент эльфийского оружия
 * Здесь тоже самое.
 */
UCLASS()
class PATTERNS_API AElvenWeapon : public AItem, public FWeaponInfo
{
	GENERATED_BODY()
	
public:	
	virtual void StoreItem_BP() override;
	virtual void UseItem_BP() override;
	virtual void SetupWithInfo(void* InfoPtr) override;
};

inline void AElvenWeapon::SetupWithInfo(void* InfoPtr) {
	auto Weapon = dynamic_cast<FWeaponInfo*>(InfoPtr);
	if(Weapon) {
		Damage = Weapon->Damage;
		Durability = Weapon->Durability;
	}
		
}