//AMyItem - это предмет, в нашем случае - скин(кожа).
//Наследуется  от FSavableActor и у него есть реализация  метода SaveData() - записать данные о себе.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SavableActor.h"
#include "GameSaver.h"
#include "MyItem.generated.h"

UCLASS()
class PATTERNS_API AMyItem : public AActor, public FSavableActor
{
	GENERATED_BODY()
	
public:	
	FName GetSkinName() const;
	virtual void SaveData(UGameSaver* Saver) override;

private:
	FName SkinName{NAME_None};
	float Damage{ 0.f };
};

inline FName AMyItem::GetSkinName() const{
	return SkinName;
}

//Поскольку AMyItem знает, что к нему придет посетитель, реализующий метод SaveItem(),
//сотв-но он вызывает в Посетителе именно этот метод. И передает в кач-ве аргумента -самого себя.
//Чтобы эти данные могли быть сохранены.
inline void AMyItem::SaveData(UGameSaver *Saver){
	if(Saver)
		Saver->SaveItem(this);
}
