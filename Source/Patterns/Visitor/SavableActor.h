//Класс FSavableActor Записываемый, сохраняемый Актор
//кот-й имеет 2 абстрактных метода - запись данных и загрузка данных -
//У него 2 Актора наследника - Игрок (AMyPlayer) и  предмет (AMyItem), которые переопределяют и реализуют эти методы

//И он принимает абстактного посетителя UGameSaver

//Данный класс можно было бы организовать в виде интерфейса. И сточки зрения UE -так лучше!
//В таком случае в SaveGame здесь: for (FActorIterator Iter{Saver->GetWorld()}; Iter; ++Iter)
//мы не искали бы акторов, кот-е наследуются у данного класса, а искали бы акторов, кот -е реализуют какой-то интерфейс.
//например так:
// for (AActor* Actor : FoundActors)
// {
// 	// Проверяем, реализует ли актор указанный интерфейс
// 	if (Actor->GetClass()->ImplementsInterface(UYourInterface::StaticClass()))
// 	{
// 		// Приводим актор к интерфейсу и выполняем необходимые действия
// 		IYourInterface* YourActorAsInterface = Cast<IYourInterface>(Actor);
// 		if (YourActorAsInterface)
// 		{
// 			// Выполняем действия с актором, реализующим интерфейс
// 			YourActorAsInterface->LoadData(Instance);
// 		}
// 	}
// }
//Здесь UYourInterface - это был бы класс нашего интерфейса, а IYourInterface - это тип указателя на интерфейс.
//Внутри цикла мы проверяем, реализует ли каждый актор в массиве указанный интерфейс с помощью функции ImplementsInterface().
//Затем мы приводим каждый актор к интерфейсу с помощью функции Cast<IYourInterface>() и,
//если успешно, выполняем необходимые действия с актором, реализующим интерфейс.

//Но для простоты написания  - это обычный класс. 

#pragma once

#include "CoreMinimal.h"

class UGameSaver;

class PATTERNS_API FSavableActor
{
public:
	virtual void SaveData(UGameSaver* Saver) = 0;
	virtual void LoadData(UGameSaver* Saver) = 0;
};
