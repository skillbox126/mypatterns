//Класс игрового объекта - враг. 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enemy.generated.h"

UCLASS()
class PATTERNS_API AEnemy : public AActor
{
	GENERATED_BODY()
	
public:
	AEnemy();

	// метод Die(), который будет вызываться при смерти врага.
	void Die();
};
