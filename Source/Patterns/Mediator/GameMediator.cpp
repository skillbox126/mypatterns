// Fill out your copyright notice in the Description page of Project Settings.


#include "GameMediator.h"
// #include "MPlayer.h"
// #include "Enemy.h"

UGameMediator::UGameMediator()
{
}

void UGameMediator::RegisterPlayer(AMPlayer* Player)
{
  Players.Add(Player);
}

void UGameMediator::RegisterEnemy(AEnemy* Enemy)
{
  Enemies.Add(Enemy);
}

void UGameMediator::NotifyPlayerDeath(AMPlayer* Player)
{
  // Реализаация логики для обработки смерти игрока
}

void UGameMediator::NotifyEnemyDeath(AEnemy* Enemy)
{
  // Реализаация логики для обработки смерти врага
}




