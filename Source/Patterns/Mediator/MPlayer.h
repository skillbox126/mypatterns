//Класс игрового объекта - Игрок. 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MPlayer.generated.h"

UCLASS()
class PATTERNS_API AMPlayer : public AActor
{
	GENERATED_BODY()
	
public:
	AMPlayer();
	
	//метод Die(), который будет вызываться при смерти игрока.
	void Die();
	
};
