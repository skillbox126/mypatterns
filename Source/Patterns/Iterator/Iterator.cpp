// Fill out your copyright notice in the Description page of Project Settings.


#include "Iterator.h"

// Реализация методов класса ArrayIterator
//Здесь - инициализация полей класса ArrayIterator с помощью списков инициализации.
//Конструктор класса ArrayIterator принимает указатель (на указатель) на массив объектов типа UObject и размер этого массива. 
// С помощью списков инициализации происходит присваивание переданных значений параметрам класса.
ArrayIterator::ArrayIterator(UObject** InArray, int32 InSize)
    : Array(InArray)
    , Size(InSize)
    , CurrentIndex(0)
{
  //или так:
  // Array = InArray;
  // Size = InSize;
  // CurrentIndex = 0;
}

void ArrayIterator::First()
{
  CurrentIndex = 0;
}

void ArrayIterator::Next()
{
  CurrentIndex++;
}

//Возвращает булевое значение (true или false) в зависимости от условия CurrentIndex >= Size.
// В данной реализации метода IsDone() проверяется, достигнут ли итератор конца коллекции (массива).
// Если текущий индекс CurrentIndex станет больше или равен размеру Size (количество элементов в массиве),
// то метод вернет true, что означает, что итератор достиг конца коллекции.
// В противном случае, метод вернет false, указывая на то, что итератор еще не достиг конца и можно продолжать итерацию.
bool ArrayIterator::IsDone() const
{
  return CurrentIndex >= Size;
}

//Возвращает элемент коллекции UObject* с индексом CurrentIndex
UObject* ArrayIterator::GetCurrentItem() const
{
  if (!IsDone())
  {
    return Array[CurrentIndex];
  }
  return nullptr;
}

// Реализация методов класса GameObjectCollection

//Здесь создается и возвращается новый объект класса ArrayIterator, который используется для итерации по коллекции объектов Objects.
// - Objects.GetData() возвращает указатель на данные (элементы) в коллекции Objects.
// - Objects.Num() возвращает количество элементов в коллекции Objects.

// Т.о., метод CreateIterator() создает новый объект класса ArrayIterator, передавая ему указатель на данные коллекции
// и количество элементов в ней. После создания объекта итератора, метод возвращает указатель на него типа IIterator*,
// который позволяет использовать этот итератор для итерации по объектам в коллекции Objects.

// В итоге, данный метод используется для получения итератора, который позволит эффективно перебирать элементы коллекции объектов Objects
//в классе GameObjectCollection.
IIterator* GameObjectCollection::CreateIterator()
{
  return new ArrayIterator(Objects.GetData(), Objects.Num());
}

//добавляем объект в коллекцию
void GameObjectCollection::AddObject(UObject* Object)
{
  Objects.Add(Object);
}

