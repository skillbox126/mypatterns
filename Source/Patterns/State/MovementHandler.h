//Для работы с описанными состояниями у нас есть класс UMovementHandler - его реализуем в виде компонента,
//кот-й покажет часть возможных переходов из одного состояния в др.

//Важно! -Данный пример не является конечным автоматом, т,к. здесь не учтены все возможные переходы. И не все они описаны.
//Можно расширить его до полноценной машины состояний, используя рассматриваемый паттерн. Учитывая все переходы и возможные
//вариации состояний. 

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MoveState.h"
#include "MovementHandler.generated.h"

/** A component that shall forward player's input to the movement state playr's in.
 * Компонент, который должен перенаправлять вводимые игроком данные в состояние перемещения, в котором находятся игроки.
 * @note This exumple doesn't implement all the transition between states possible.
 * В этом примере реализованы не все возможные переходы между состояниями.
 */
UCLASS(meta=(BlueprintSpawnableComponent) )
class PATTERNS_API UMovementHandler : public UActorComponent
{
	GENERATED_BODY()

public:	
	/** @brief Override to do initial setup. Переопределите, чтобы выполнить первоначальную настройку.
	 */
	//Содержит перегрузку BeginPlay() чтобы инициировать необходимое стартовое состояние. А так же привязать
	//события вывода к нашим методам, которые будут реагировать на события:
	//- Добавляем начальное состояние покоя. И задает его в качестве текущего состояния.
	//- Привязываем входные данные игрока к нашим функциям.

	virtual void BeginPlay() override;

	/** @brief Set initial state. Установит начальное состояние в Idle.
	 */
	void InitState();

	/** @brief Here we bind player inputs to our functions. Здесь мы привязываем входные данные игрока к нашим функциям.
	 */
	void BindInputs();

protected:
	/** @brief Adds new state of given type to our collection. Добавляет новое состояние заданного типа в нашу коллекцию.
	 */
	//Здесь служебный метод AddState()  - по передаваемому Enum будет добавлять новое состояние в коллекцию (массив) состояний.
	//+ Здесь происходит подписка на делегат
	void AddState(EMoveState Type);

	/** @brief Change current state to the one on given type. Измените текущее состояние на состояние для данного типа.
	 */
	//Здесь мы реализвываем переход из одного состояния в другое. Набор методов, со строки, кот-я реагирует на событие ввода
	// - движение по осям и разворот, остановку, так и на события нажатия клавиш и отпускания клавиш.
	void ApplyState(EMoveState Type);
protected:
	
	/** @brief We want to know if same state calls for transition. Мы хотим знать, требует ли одно и то же состояние перехода.
	 */
	//Здесь метод, подписывающийся на делегат события инициализации смены состояния из нашего EMoveState
	UFUNCTION()
	void OnCurrentStateChanged(EMoveState NewState);

	/** @brief Move Forward.
	 */
	UFUNCTION()
	void OnMoveAlongX(float Value);

	/** @brief Move Sideways.
	 */
	UFUNCTION()
	void OnMoveAlongY(float Value);

	/** @brief Simply rotate. Просто поверните.
	 */
	UFUNCTION()
	void OnRotateAroundZ(float Value);

	/** @brief Stop all movement.
	 */
	UFUNCTION()
	void OnStop();

	/** @brief React to run button press. Реагируйте на нажатие кнопки запуска.
	 */
	UFUNCTION()
	void OnRunBtnPressed();

	/** @brief React to run button release. Реагируйте на нажатие кнопки запуска.
	 */
	UFUNCTION()
	void OnRunBtnReleased();

private:
	/** @brief A collection of unique movement states player can be in.
	 * Коллекция уникальных состояний перемещения, в которых может находиться игрок.
	 */
	//Коллекция состояний в виде массива.
	TArray<FMoveState> States;

	/** @brief Current movement state player's in. Текущее состояние перемещения, в котором находится игрок.
	 */
	//Текущее состояние, к которому будет применяться событие ввода.
	FMoveState* CurrentState; //укаазтель!

	/** @brief Save it in case we want to restore this specific state.
	 * Сохраните его на случай, если мы захотим восстановить это конкретное состояние.
	 */
	//Для простоты - нажата ли клавиша бега или нет.
	bool bIsRunning { false };
};

//Перегрузка BeginPlay() чтобы инициировать необходимое стартовое состояние. А так же привязать
//события вывода к нашим методам, которые будут реагировать на события.
inline void UMovementHandler::BeginPlay(){
	Super::BeginPlay(); //из ActorComponent
	InitState();
	BindInputs();
}

//Добавляет начальное состояние покоя. И задает его в качестве текущего состояния.
inline void UMovementHandler::InitState()
{
	AddState(EMoveState::EMS_Idle);
	CurrentState = &States.Top(); //указатель на адрес - FMoveState* CurrentState = &States.Top();
	
}


inline void UMovementHandler::BindInputs()
{
	//Здесь мы привязываем входные данные игрока к нашим функциям.
}

//AddState()  - по передаваемому Enum будет добавлять новое состояние в коллекцию состояний.
//И + Здесь происходит подписка на делегат OnStateChanged - для того, чтобы знать, не вызвало
//ли какое-то из состояний оповещения о том, что оно должно быть изменено.
//Это обращение мы будем обрабатывать в OnCurrentStateChanged
inline void UMovementHandler::AddState(EMoveState Type) {
	auto Char = dynamic_cast<ACharacter*>(GetOwner());
	if(Char) {
		States.AddUnique(FMoveState{ Type, dynamic_cast<APlayerController*>(Char->GetController()) });

		States.Top().OnStateChanged.AddDynamic(this, &UMovementHandler::OnCurrentStateChanged);
		//States.Top() мы обращаемся к последнему элементу массива состояний (States).
		// Top() возвращает ССЫЛКУ на последний элемент массива( т.е. его псевдоним), а не копию этого элемента.
		//Адрес ссылки - это адрес самого объекта -  &States.Top()
	}
}


// inline void UMovementHandler::AddState(EMoveState Type) {
// 	auto Char = Cast<ACharacter>(GetOwner());
// 	if (Char) {
// 		APlayerController* PlayerController = Cast<APlayerController>(Char->GetController());
// 		if (PlayerController) {
// 			FMoveState NewState(Type, PlayerController);
// 			// NewState.OnStateChanged.AddDynamic(this, &UMovementHandler::OnCurrentStateChanged);
// 			// NewState.OnStateChanged.BindUFunction(this, FName("OnCurrentStateChanged"));
// 			NewState.OnStateChanged.BindLambda([this](EMoveState State){
// 				OnCurrentStateChanged(State);
// 			});
// 			
// 			States.AddUnique(NewState);
// 		}
// 	}
// }


//Здсь маленькая лямбда - Будем искать состояние по каждому типу, указанному в коллекции состояний.
//Отметим -что можно было бы для простоты не делать GetState(), а в FMoveState - просто перегрузить оператор сравнения для того
//чтобы он работал с Enum. При желании можно избавиться описанным способом от метода GetState().
static FMoveState* GetState(TArray<FMoveState>& States, EMoveState Type) {
	// FindByPredicate - это метод контейнера, который принимает предикат (функцию или лямбда-выражение)
	// в качестве аргумента, который возвращает значение true, если элемент удовлетворяет заданному условию, и false в противном случае.

	// [=](FMoveState& State) {...} - это лямбда-выражение, которое передается в качестве предиката методу FindByPredicate. Разберем его части:
	// - [=] - захват всех локальных переменных по значению внутри лямбда-выражения.
	// - (FMoveState& State) - это аргумент для лямбда-выражения, который предполагает, что каждый элемент States будет передаваться в выражение
	// как FMoveState& (ссылка на FMoveState).
	// - {...} - это тело лямбда-выражения, где содержится код, который проверяет условие для каждого элемента States.
	// Если условие истинно, метод возвращает этот элемент.

	// т.е. проходим по каждому элементу States и вызывает лямбда-выражение для каждого элемента, передавая ссылку на элемент. 
	// Если лямбда-выражение возвращает true для конкретного элемента, то этот элемент считается найденным, и он будет сохранен в переменной Result.
	FMoveState* Result = nullptr;
	Result = States.FindByPredicate([=](FMoveState& State)
	{
		return  State.GetStateType() == Type; //тело функции для лябда-выражения. Только зачем здесь тавтология == Type? они и так будут равны!
	});
	return Result;
	//Ни ФИГА!
	//Если без сравнения -  лямбда-выражение возвращает вызов метода GetStateType() объекта State,
	//но возвращаемое значение не сравнивается с каким-либо другим значением.
	//В данном случае, возвращается значение типа состояния, но это не то, что требуется от поиска по предикату.

	// FindByPredicate требует предикат, который возвращает логическое значение (например, true или false), для фильтрации элементов,
	// ПОЭТОМУ следует вернуть результат сравнения State.GetStateType() с нужным типом.
	// В противном случае, возвращаемое значение предиката не будет корректно определять, соответствует ли текущий элемент условию поиска.

	//TODO Но если результат поиска будет отрицательным? НЕ следует ли сначала объявить FMoveState* Result = nullptr;?
}

//Здесь получаем некот-е состояние по его типу и делаем его текущим состоянием. А если не нашли такого состояния, то
//мы его добавляем в нашу коллекцию. И затем делаем его текущим состоянием.Здесь происходит ленивая инициализация,
//если мы никогда ранее не находились в таком состоянии.
inline void UMovementHandler::ApplyState(EMoveState Type){
	CurrentState = GetState(States, Type);
	if(!CurrentState) {
		//Same lazy initialisation taking place here. Здесь происходит та же ленивая инициализация.
		AddState((Type));
		CurrentState = &States.Top();
	}
	//здесь вызываем текущее состояние, кот-е будет сигнализировать о том, что это сотояние начало свою работу.
	CurrentState->ApplyState(); 
}


//Здесь небольшой костыль, кот-й свойственен реализации машинного состояния. Это последовательность if - else или
//свичей для проверки изменения состояния. Паттерн Состояние призывает исп-ть вот такие классы или структуры для того,
//чтобы избегать многоуровневой конструкции if - else и повторяющихся вызовов. Поэтому при желании эту часть
//кода можно изменить.

//В нашем случае мы их используем для того, чтобы проверить после окончания нашего истощения - Должны ли мы перейти
//в состояние бега bIsRunning? Или же мы можем перейти в состояние покоя.

//Почему мы не переходим в состояние ходьбы? Потому что в данном случае в следующем же кадре и даже в текущем -
//мы получим событие о вводе движения. И наша анимация при правильной реализации совершит бесшовный переход.
//Т.е. мы даже не заметим, что мы перешли в состояние покоя. И у нас анимация будет себя вести совершенно адекватно.
inline void UMovementHandler::OnCurrentStateChanged(EMoveState NewState)
{
	if(NewState == EMoveState::EMS_ExhaustEnd)
	{
		if(bIsRunning) //если в состоянии бега
		{
			NewState = EMoveState::EMS_Run; //бежим
		}
		else //если нет -встаем
		{
		  NewState = EMoveState::EMS_Idle;
		}
	}
	//если же мы получили состояние начала истощения - мы принудительно отключаем переменную бега и персонаж уже не может бежать.
	else if(NewState == EMoveState::EMS_ExhaustBegin)
	{
	  bIsRunning = false;
	}
	//после этого вызываем применение нового состояния.
	ApplyState(NewState);
}

//Далее идет событие обработки ввода: движение по осям, бег, вращение, остановку при соблюдении нажатия клавиш. Все это
//инкапсулировано в маленькие вызовы ф-й. Это позволяет избежать огромных конструкций для сравнения возможных состояний
//всех возможных переходов.
inline void UMovementHandler::OnMoveAlongX(float Value){
	if(CurrentState)
		CurrentState->MoveAlongX(Value);
}

inline void UMovementHandler::OnMoveAlongY(float Value){
	if(CurrentState)
		CurrentState->MoveAlongY(Value);
}

inline void UMovementHandler::OnRotateAroundZ(float Value){
	if(CurrentState)
		CurrentState->RotateAroundZ(Value);
}

inline void UMovementHandler::OnStop(){
	if(CurrentState)
		CurrentState->Stop();
}

inline void UMovementHandler::OnRunBtnPressed(){
	if(CurrentState && CurrentState->GetStateType() != EMoveState::EMS_ExhaustBegin) {
		ApplyState(EMoveState::EMS_Run);
		bIsRunning = true;
	}
}

inline void UMovementHandler::OnRunBtnReleased(){
	if(CurrentState && CurrentState->GetStateType() == EMoveState::EMS_Run) {
		ApplyState(EMoveState::EMS_Idle);
		bIsRunning = false;
	}
}

//Применение паттерна Состояние.
// - когда поведение объекта кардинально меняется в зави-ти от того, в каком он нах-ся состоянии. Особенно
//если типов состояний много bool, мало того, что эти состояния меняются, так еще и реализация этих
//состояний может меняться. Поэтому оч удобно выделить их в отдельные сущности.
// - нужно исп-ть когда замечаем, что становится много условных операторов, кот-е выбирают поведение
//в зав-ти от текущих значений нек-х полей класса.
//- нельзя забывать о случае, когда используется полнофунционирующая машина состояний и нам необходимо реализовать
//похожее состояние, либо множество переходов. В этом случае можно с пом-ю паттерна Состояние
//создать иерархическую машину состояний, базирующуюся на наследовании. И похожие состояния могут быть
//наследниками некот-го родительского класса.

//Плюсы:
// - Избавляет от множества условных операторов и свичей машины состояний.
// - Концентрирует код, связанный с одним состоянием, вместе. Значительно упрощается код контекста. Т.е в нем
//мы просто вызываем одни и те же методы и нам не важно, что происходит внутри.

// Минусы
// - может неоправдано усложнить код. И в нек-х случаях гораздо более выгодно оставить последоват-ть if-else, поскольку
//реализация данного Паттерна может усложнить код.