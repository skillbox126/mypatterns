//Паттерн Состояние (State) - поведенческий паттерн

// - позволяет объектам менять поведение в зависимости от состояния, в кот -м они находятся.
//При этом может показаться, что изменился класс объекта и вся его сущность.

//Данный паттерн невозможно рассматривать в отрыве от известной концепции машино-состояний или конечный автомат, в кот-м
//описывается нек-й предопределенный и законченный набор состояний и все возможные переходы между данными состояниями из одного в другое.

//Находясь в разных состояниях нек-е эл-ты прг будут по разному реагировать на одинаковые события внешней среды.
//Поэтому паттерн Состояние будет исп-ся в тех случаях, когда требуется различное поведение объекта в зависимости от контекста.

//Клиент (Contex) будет хранить ссылку на текущее состояние и вызывать известный набор действий, кот-й не будет меняться,
//либо при изменении будет гарантировать, что все состояния должны уметь реагировать на эти действия.
//Т.о. структура паттерна Состояния достаточно простая, что нельзя сказать про логику его использования.

//Один из 1-х случаев, когда мы столкнемся с паттерном Состояние в игровой разработке -это организация движения персонажа.
//Причем, реализация движения с помощью логики - это такие переходы из одного состояния движения в другое.

//Рассмотрим на примере кода, описывающего ряд состояний: Idle - или состояния покоя, ходьбы, падения либо полета.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameFramework/Character.h"
#include "GameFramework/PlayerController.h"
#include "MoveState.generated.h"

class ACharacter;
class APlayerController;

/** @breaf Enum to represent all available states. Перечисление для представления всех доступных состояний.
 */

UENUM(BlueprintType)
enum class EMoveState : uint8
{
	EMS_Idle = 0,
	EMS_Walk = 1,
	EMS_Run = 2,
	EMS_ExhaustBegin = 3, //Exhaust - истощение - здесь начинается истощение. Во время продолжит-го бега может проявляться какими-то эффектами-
	EMS_ExhaustEnd = 4, //одышка, замедление хотьбы итд.
	EMS_Fall = 5 //падение
};

//Здесь структура, кот-я описывает реакцию на некот-е действия в рамках контекста.
/** @breaf A base state for other objects to work with. Базовое состояние для работы с другими объектами.
 */
USTRUCT(Blueprintable, BlueprintType)
struct PATTERNS_API FMoveState
{
	GENERATED_BODY()

public:
	/** @brief Fires once state will be chenged. Срабатывает, как только состояние будет изменено.
	*/
	  DECLARE_MULTICAST_DELEGATE_OneParam(FStateChange, EMoveState, NewState); //TODO !!!  - NewState не надо?
	// DECLARE_MULTICAST_DELEGATE_OneParam(FStateChange, EMoveState);
	FStateChange OnStateChanged;

// public:


	//Перегрузка оператора сравнения для UENUM по значению
	bool operator==(const FMoveState& State, EMoveState EnumState)
	{
		return State.GetStateType() == EnumState;
	}

	//FMoveState MyState = ...; // как-то определено состояние
	// if (MyState == EMoveState::EMS_Walk)
	// {
	// 	
	// }


	
	/** @brief We override ctor to impliment data load from some data table.     
	 * Мы переопределяем ctor, чтобы реализовать загрузку данных из некоторой таблицы данных.
	*/
	//Здесь конструктор. Он принимает ENUM типы, принимает контроллер, кот-й пригодится для выполнения действий.
	explicit FMoveState(EMoveState Type, APlayerController* Controller) : StateType(Type), Owner(Controller)
	{
		FMoveState::LoadStateInfo(); //+ инициирует загрузку данных для текущего состояния из внешней таблицы.
	}

	FMoveState() = default; // Генерация конструктора по умолчанию

	virtual ~FMoveState() = default;

	/** @brief Enum type bound to this state. Возвращает Enum type -  StateType, привязанный к этому состоянию.
	 */
	EMoveState GetStateType() const; 

	
	//Здесь от ApplyState() до Stop() - виртуальные методы, кот-е определяют реакцию на контекст

	/** @brief Any methods that shall be called once this event is set mast be here.
	 * Любые методы, которые должны быть вызваны после установки этого события, должны быть здесь.
	 */
	//Здесь метод, кот-й мы будем исп-ть для перехода в данное состояние. Т.е. у Актора есть BeginPlay(), а здесь в структуре
	//мы будем исп-ть такой ApplyState(), кот-й будет знаком того, что мы перешли в данное состояние и нам нужно
	//что-то с этим сделать.
	virtual void ApplyState()
	{
		//здесь вызываем текущее состояние, кот-е будет сигнализировать о том, что это сотояние начало свою работу.
		// Например - просто выводим сообщение о начале работы текущего состояния
		UE_LOG(LogTemp, Warning, TEXT("State %d is now active!"), static_cast<int>(GetStateType()));

		// Можно выполнить какие-то действия, связанные с началом работы состояния -Например,
		// изменить какие-то параметры объекта в зависимости от текущего состояния
	}

	/** @brief Move Forward. 
	 */
	//MoveAlongX() - движение вдоль оси Х. В UE по дефолту ось Х это ось по направлению вперед и назад. Данный метод, при наличии
	//контроллера  - задавать движение с определенной скоростью, кот-ю мы получим при загрузке наших параметров.
	//Скорость может быть как положит-й, так и отрицат-й. Соответственно движемся вперед или назад.
	virtual void MoveAlongX(float Value)
	{
		if(GetOwner())
		{
			Value *= SpeedX * GetOwner()->GetWorld()->GetDeltaSeconds();
			// const FVector Direction = FRotationMatrix(GetControllerYaw()).GetUnitAxes(EAxis::X); //TODO здесь ошибка

			//Исправление
			FRotator Rotation = GetControllerYaw();
			FRotationMatrix RotMatrix(Rotation);
			FVector Direction = RotMatrix.GetUnitAxis(EAxis::X);

			GetOwner()->GetCharacter()->AddMovementInput(Direction, Value);
		}
	}

	/** @brief Move sideways.
	 */
	//По Y  - аналогично. Движение вправо-влево.
	virtual void MoveAlongY(float Value)
	{
		if(GetOwner())
		{
			Value *= SpeedY * GetOwner()->GetWorld()->GetDeltaSeconds();
			//Здесь SpeedY -скорость движения по оси Y, а Value - для определения величины перемещения.
			//Умножение на GetOwner()->GetWorld()->GetDeltaSeconds() учитывает время между кадрами (дельта времени),
			//чтобы сделать перемещение более плавным.

			// const FVector Direction = FRotationMatrix(GetControllerYaw()).GetUnitAxes(EAxis::Y);

			FRotator Rotation = GetControllerYaw(); //Получаем угол поворота (yaw) контроллера.
			//Это позволяет определить направление движения персонажа в горизонтальной плоскости.

			FRotationMatrix RotMatrix(Rotation); //Создаем матрицу поворота на основе полученного угла поворота.

			FVector Direction = RotMatrix.GetUnitAxis(EAxis::Y); //Получаем единичный вектор направления движения по оси Y из матрицы поворота.
			//Это позволяет определить направление, куда должен двигаться персонаж в плоскости.

			GetOwner()->GetCharacter()->AddMovementInput(Direction, Value); //Добавляем Value векторного направления Direction
			//к движению персонажа. То есть, это применяет силу движения к персонажу, направленную в рассчитанном направлении.

		}
	}

	/** @brief Simple rotation.
	 */
	//Здесь вращение вокруг своей оси.
	virtual void RotateAroundZ(float Angle)
	{
		if(GetOwner())
		{
			Angle *= SpeedZ * GetOwner()->GetWorld()->GetDeltaSeconds();
			GetOwner()->AddYawInput(Angle);
		}
	}

	/** @brief By default sets state to idle. По умолчанию устанавливает состояние в режиме ожидания.
	 */
	virtual void Stop()
	{
		OnStateChanged.Broadcast(EMoveState::EMS_Idle);
	}
	
protected:
	/** @brief Owning player controller. Владелец игровым контроллером.
	 */
	APlayerController* GetOwner() const
	{
		return Owner;
	}

	/** @brief Current controller yaw rotation. Текущий регулятор поворота по рысканию.
	 */
	FRotator GetControllerYaw() const
	{
		FRotator Rot{FRotator::ZeroRotator};
		if(Owner)
			Rot = FRotator(0.f, Owner->GetControlRotation().Yaw, 0.f);
		return Rot;
		// метод GetControllerYaw() возвращает объект типа FRotator,
		//и его значение можно использовать напрямую в качестве параметра FRotationMatrix().
		//После этого вызов будет корректно возвращать единичный вектор по оси X или Y из матрицы поворота.
	}

	/** @brief Assume we load into from some data-table or else.
	 * Предположим, мы загружаем данные из какой-либо таблицы данных или еще откуда-нибудь.
	 */
	virtual void LoadStateInfo();

private:
	/** @brief Enum representation of the state. Enum представительство состояний.
	 */
	//Сохраним переданный тип Enum, для того, чтобы можно было с помощью него выполнять сравнения.
	EMoveState StateType;

	/** @brief Character owning player controller. Персонаж, владеющий игровым контроллером.
	 */
	//APlayerController нужен чтобы выполнять движения.
	UPROPERTY()
	APlayerController* Owner{ nullptr };

	//Пременные скоростей SpeedX, SpeedY, SpeedZ мы будем получать из LoadStateInfo() - из внешней таблицы.
	//Для разных состояний значения этих перемнных будут варьироваться.
	/** @brief Forward movement speed. Скорость движения вперед.
	 */
	float SpeedX = 0.f;

	/** @brief Sideways movement speed. Скорость бокового перемещения.
	 */
	float SpeedY = 0.f;

	/** @brief Rotation speed. Скорость вращения.
	 */
	float SpeedZ = 0.f;
};



//Если персонаж нах-ся в состоянии покоя, то при вызове метода движения по осям Х, Y  - вызываются методы суперкласса.
//И одновременно мы через Broadcast информируем подписчиков о том, что мы должны изменить наше состояние  - минимум -
//до состояния ходьбы - EMS_Walk
USTRUCT()
struct FIdleState : public FMoveState
{
	GENERATED_BODY()
public:
	virtual void MoveAlongX(float Value) override
	{
		OnStateChanged.Broadcast(EMoveState::EMS_Walk);
		Super::MoveAlongX(Value);
	}

	virtual void MoveAlongY(float Value) override
	{
		OnStateChanged.Broadcast(EMoveState::EMS_Walk);
		Super::MoveAlongY(Value);
	}

	/** @brief Whill idle we do nothing to stop. Находясь в бездействии, мы ничего не делаем, чтобы остановиться.
	 */
	//Если будет вызван метод Stop(), то в состоянии покоя мы не должны совершать никаких доп действий -
	// - оповещать о переходе в состояние покоя, т.к. мы и так в нем находимся.
	virtual void Stop() override{}
};

//Состояние хотьбы будет отличаться только значениями скоростей по осям.
USTRUCT()
struct FWalkState : public FMoveState {
	GENERATED_BODY()
};


//В состоянии бега будем обновлять значения выносливости - Stamina.
USTRUCT()
struct FRunState : public FMoveState {
	GENERATED_BODY()
public:
	virtual void MoveAlongX(float Value) override
	{
		Super::MoveAlongX(Value);
		UpdateStamina();
	}

protected:
	/** @brief Override it to get additional fieleds. Переопределите его, чтобы получить дополнительные поля.
	 */
	//Перегружаем LoadStateInfo() для того, чтобы получить значения доп-х полей - получаем максимальное значение - StaminaMax.
	virtual void LoadStateInfo() override;

	/** @brief We wont to apdate to our stamina each frame. Мы обычно обновляем нашу выносливость в каждом кадре.
	 */
	//Допустим у нас есть нек-е значение Stamina, кот-я на дельта Х будет уменьшаться с каждым кадром
	void UpdateStamina()
	{
		Stamina -= GetOwner()->GetWorld()->GetDeltaSeconds();
		if(Stamina <= 0.f)
		{
			OnStateChanged.Broadcast(EMoveState::EMS_ExhaustBegin); //меняем состояние на начало истощения
			Stamina = StaminaMax; //Поскольку мы уже находимся в другом состоянии  -конфликта не будет
			//если мы здесь сделаем это.
		}
	}

private:
	/** @brief Full stamina value. Полное значение выносливости.
	 */
	float StaminaMax = 0.f;

	/** @brief Current stamina value.
	 */
	float Stamina = 0.f;
};

//геттер для получения текущего состояния
inline EMoveState FMoveState::GetStateType() const{
	return  StateType;
}


//Состояние истощения . При этом состояние окончания истощения не предусмотрено. Потому что данное состояние будет служить нам
//сигналом о том, что следует перейти в како-то другое состояние. Мы увидим это дальше -когда будем рассамтривать контроллер
//всех этих наших состояний обработчиков.
USTRUCT()
struct FExhoustedState : public FMoveState
{
	GENERATED_BODY()
public:
	//После того, как мы перешли в состяние истощения - мы начинаем восстановление.
	virtual void ApplyState() override
	{
		StartCoolDown();
	}

	/** @brief Once in this state we shall start counting down cooldown time.
	 * Оказавшись в этом состоянии, мы начнем обратный отсчет времени остывания.
	 */
	//Здесь запускаем таймер и через n секунд будет вызван OnCooldown_Callback(), кот-й 
	void StartCoolDown();

protected:
	/** @brief Override it to get additional fields. Переопределите его, чтобы получить дополнительные поля.
	 */
	//Опять перегружаем LoadStateInfo() чтобы получить время CooldownTime()
	virtual void LoadStateInfo() override;

	/** @brief Once cooled down call this. Как только остынет, назовите это.
	 */
	//Здесь мы говорим о том, что наш персонаж оттдохнул и восстановился. Соответственно необходимо изменить
	//состояние на любое другое.
	void OnCooldown_Callback()
	{
		OnStateChanged.Broadcast(EMoveState::EMS_ExhaustEnd);
	}
private:
	/** @brief Cooldown duration. Продолжительность перезарядки выносливости.
	 */
	float CooldownTime{ 0.f};
};

//Состояние падения -здесь мы не будем делать обращения к методам суперкласса. Т.к. здесь мы не должны будем обрабатывать
//никакой ввод. Персонаж не сможет ничего сделать. Двигаться итд.
USTRUCT()
struct FFallState : public FMoveState
{
	GENERATED_BODY()
public:
	/** @brief This state won't call any parents functionts, couse no input shall be applied.
	 * Это состояние не вызовет никаких родительских функций, потому что никакие входные данные применяться не будут.
	 */
	virtual void MoveAlongX(float Value) override {}
	virtual void MoveAlongY(float Value) override {}
	virtual void RotateAroundZ(float Value) override {}
};

//Для работы с описанными состояниями у нас есть класс UMovementHandler - его реализуем в виде компонента