// Fill out your copyright notice in the Description page of Project Settings.


#include "MoveState.h"
//
//
// MoveState::MoveState()
// {
// }
//
// MoveState::~MoveState()
// {
// }
void FMoveState::LoadStateInfo()
{
}

void FRunState::LoadStateInfo()
{
FMoveState::LoadStateInfo();

  //получаем максимальное значение - StaminaMax.
}

void FExhoustedState::StartCoolDown()
{
  //запускаем таймер и через n секунд будет вызван OnCooldown_Callback()
}

void FExhoustedState::LoadStateInfo()
{
  FMoveState::LoadStateInfo(); // Вызываем базовую версию функции LoadStateInfo()

  // Добавляем дополнительную логику для получения и использования CooldownTime()
  float Cooldown = CooldownTime; // Получаем время задержки CooldownTime()
  UE_LOG(LogTemp, Warning, TEXT("Cooldown time for this state: %f"), Cooldown);
}