//Класс UDamageHandler : public UActorComponent  - компонет Урона -
//получает оружие на вход и для каждой части тела он будет выполнять свою цепочку действий для конкретного урона.
//Также можно развить этот класс применением каких-то положит-х или отрицательных эффектоа.
// Это компонент, кот-й висит на нашем же акторе. И разбирается со всем,
//что связано с нанесением урона по разным частям тела.



#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Weapon.h"
#include "DamageHandler.generated.h"

class AWeapon;

/** @brief A class to Handle all the incoming damage.
 * @details Takes all the into needed from AWeapon instance and aplies shields, buffs and debuffs to calculate area damage taken.
 * Извлекает всю необходимую информацию из экземпляра оружия и применяет щиты, усиления и дебаффы для расчета нанесенного урона по площади.
 */

UCLASS()
class PATTERNS_API UDamageHandler : public UActorComponent
{
	GENERATED_BODY()

public:
	/** @brief Calculate head damage by weapon
	 */
	void DamageHead(AWeapon* Weapon);

	/** @brief Calculate body damage by weapon
	 */
	void DamageBody(AWeapon* Weapon);

	/** @brief Calculate arms damage by weapon
	 */
	void DamageArms(AWeapon* Weapon);

	/** @brief Calculate legs damage by weapon
	 */
	void DamageLegs(AWeapon* Weapon);


	//Итог:
	// Паттерн Адаптер может исп-ся в таких случаях:
	// - Когда мы хотим использовать какой-то класс, но его интерфейс не соответствует уже созданному коду. Но это 
	//взаимодиействие необходимо реализовать.
	// - Адаптер позволяет дополнять функциональность, которой нам не хватает.
	// - Для того, чтобы не дублировать один и тот же код в ветках подклассов, мы могли бы создать объект Адаптер,
	//кот-й помимо реализации методов супер-класса добавлял бы требуемую функциональность.

	// Преимущества:
	// - клиенту нет необходимости о преобразовании одного интерфейса в другой.

	// Минус
	// - появление новых сущностей, доп классов -а это лишнее время на разработку, рефакторинг и поддержание - усложнение прг.
};
