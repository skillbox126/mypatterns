//Паттерн Адаптер - Структурный паттерн

//- позволяет совместно работать с объектам с несовместимыми интерфейсами.

//Допустим - есть некот-я сущность, кот-я представляет данные в удобном и логичном для нее виде. Однако эти данные требуются
//в другом месте и в другом формате, представлении.

//Например. Источник данных в формате XML, а сторонняя библиотека хочет получить на вход данные формата JSON.
//Именно паттерн Адаптер должен решить проблему - взяв интерфейс одной сущности - транслировать его в интерфейс другой сущности,
//и реализовать все необходимые вызовы. Чтобы 2 эти сущности могли работать друг с другом как в одном направлении, так и в обратном.
//- Двухсторонние адаптеры.

//Структура паттерна Адаптер.
//Есть неск-ко вариантов- Реализация с использованием наследования, либо Агрегация.
//Т.е. у нас есть нек-й интерфейс Адаптера, кот-й реализует методы, необходимые для клиентов. И у нас есть нек-й Конкретный Адаптер.
//Он связывается с сущностью, данные которой мы будем адаптировать под наши нужды. Если же мы будем подходить к этому с точки зрения
//наследования, то данный Конкретный Адаптер будет наследником данного интерфейса и адаптируемой сущности. В этом вся разница.

//Пример применения Адаптера рассмотрим на примере Resolving a collision Разрешения коллизий.
//В какой-то мере уже сталкивались с Адаптером в Прокси. А сейчас посмотрим на еще одну ситуацию, когда потребуется Адаптер.

//Допустим есть компонет UCollisionHandler, кот-й мы навешиваем на нашего Актора. Он подписывается на события возникновения коллизий
//примитивных компонентов. Далее этот компонент будет транслировать события этих коллизий дальше  - в каком-то определенном формате.
//


#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DamageHandler.h"
#include "Weapon.h"
#include "CollisionHandler.generated.h"

class AWeapon;
class UDamageHandler;


/** @brief Collision Handler Class
 * @details Listens to the components collision events abd passes them into proper handlers.
 * Прослушивает события столкновения компонентов и передает их в соответствующие обработчики.
 * Адаптирует интерфейс компонентов столкновения к функционалу обработки урона и оружия
 */
UCLASS()

class PATTERNS_API UCollisionHandler : public UActorComponent
{
	GENERATED_BODY()

protected: //доступны только для методов в наследниках этого класса
	
	//Класс Урон UDamageHandler содержит в себе урон, кот-й мы загружаем откуда-то из таблицы. в GetDamageHandler() мы можем получить базовый
	//урон от оружия. 
	/** @return Damage Handling component
	 */
	UDamageHandler* GetDamageHandler(); //получаем указатель на класс компонента Урона UDamageHandler : public UActorComponent

	// Здесь в BeginPlay будет делать вызов на подписку на события.
	/** @return Ovveride it since (c этого момента) all the components are all ready inited at this point.
	 * Ovveride это, так как на данный момент все компоненты готовы к установке
	 */ 
	virtual void BeginPlay() override;

	//Здесь метод отвечает за привязку примитивных компонентов к нужным событиям. 
	/** @return Bind the UPrimitiveComponent overlap event.
	 */ 
	 void BindComponents();

	//Здесь метод реализует сигнатуру оверлапа, кот-я определена и объявлена в примитивных компонентах. Данный метод в нашем
	//случае как раз будет обрабатывать все входящие события пересечения коллизий. 
	/** @return Handle incoming overlap event. Обрабатывать входящее событие перекрытия.
	 * от всех примитивных компонентов.
	*/
	UFUNCTION()
	void OnActorOverlapBegin(UPrimitiveComponent* OverlappedComp,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	//Здесь служебный метод. Он будет разбираться -куда будет направлен вызов событий пересечения коллизий в зависимости от тега. 
	/** @return Choose proper function call based on the component tag.
	 * Выбор соответсвующего (правильного) вызова функции на основе тега компонента.
	*/
	void SwitchByComponentTag(UPrimitiveComponent* Comp, AWeapon* Weapon);

private:

	//Здесь же мы содержим указательна UDamageHandler. Это компонент, кот-й висит на нашем же акторе. И разбирается со всем,
	//что связано с нанесением урона.
	
	//Поскольку коллизии могут происходить часто, то в данном случае есть смысл сохранить указатель на данный объект локально.

	//Данный класс не является самой качественной вариацией класса обработчика коллизий. Потому что он обращается к конкретному
	//компоненту UDamageHandler*. Лучшая реализация содержит дальнейшее оповещение о событиях коллизии.
	//Но уже о конкретных событиях коллизии. т.е. если мы получили событие из какого-либо компонента, то мы оповещаем о том,
	//что мы получили именно какой-то конкретный специфический вид коллизии. И для этого он содержал бы особое событие,
	//на которое будет подписываться тот же самый DamageHandler.

	//Однако такая реализация уже будет не совсем Адаптером. Хотя на вход он всё так же получает значение одного типа. На выходе
	//получает значение другого типа. Но из-за того, что она в себе не содержит указатели на другие сущности,
	//это уже будет ближе к МОСТУ, чем к Адаптеру.
	
	/** @return Save reference to this component for quick access later.
	 * Сохраним ссылку на этот компонент для быстрого доступа позже.
	*/
	UPROPERTY()
	UDamageHandler* DamageHandler{ nullptr };
};


inline void UCollisionHandler::BeginPlay(){
	Super :: BeginPlay();
	BindComponents();
}

//Здесь -  если у нас нет указателя на данный объект, то мы обращаемся к нашему владельцу и у него получаем указатель на актуальную версию
//этого компонента. И возвращаем ее. Если же указатель уже есть -то просто возвращаем его.
inline UDamageHandler * UCollisionHandler::GetDamageHandler(){
	if(!DamageHandler) {
		/*UActorComponent* ActorComponent = GetOwner()->GetComponentByClass(UDamageHandler::StaticClass());
		 *Сначала Получаем владельца - ActorComponent и кастуем к наследнику
		DamageHandler = dynamic_cast<UDamageHandler*>(ActorComponent);*/

		DamageHandler = dynamic_cast<UDamageHandler*>(GetOwner()->GetComponentByClass(UDamageHandler::StaticClass()));
	}
	return DamageHandler;
}

//Здесь в BindComponents - в цикле проходимся по всем найденным примитивным компонентам на Акторе, подписываемся на их метод
//OnComponentBeginOverlap. Сигнатруры наших методов совпадают, поэтому подписываемся без проблем.
inline void UCollisionHandler::BindComponents(){
	if(GetOwner()) {
		TArray<UPrimitiveComponent*> Components; //создаем массив для всех компонентов 
		GetOwner()->GetComponents(Components); //получаем всех в массив
		// UPrimitiveComponent* Collider{ nullptr }; //не нужно
		for(auto Component : Components) {
			Component->OnComponentBeginOverlap.AddDynamic(this, &UCollisionHandler::OnActorOverlapBegin);
		}
	}
}

//Здесь служебный метод OnActorOverlapBegin проверяет, что есть компонет, с которым мы пересеклись. А так же, что владелец этого
//компонета является наследником класса оружия. Для удобства решили -что урон может получаться только от оружия.
//Если коллизия произошла с оружием, мы приходим в SwitchByComponentTag - передаем компонент, с которым мы пересеклись,
//и передаем указатель изи другого Актора, но уже в виде оружия, чтобы дальше можно было работать.
inline void UCollisionHandler::OnActorOverlapBegin(UPrimitiveComponent* OverlappedComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	// UClass* Class = OtherActor->GetClass();
	// Class->IsChildOf(const UStruct* SomeBase);//требеут структуру
	// if(OverlappedComp && OtherActor->GetClass()->IsChildOf(AWeapon::StaticClass)); //TODO Почему у него работает так?
	if(OverlappedComp && OtherActor->IsA(AWeapon::StaticClass));
	//IsChildOf() - для классов, IsA() для экземпляров класса

	{
		AWeapon* OtherActorWeapon = dynamic_cast<AWeapon*>(OtherActor);
		// OtherActor и AWeapon наследуются от AActor - поэтому приведение возможно
		SwitchByComponentTag(OverlappedComp, dynamic_cast<AWeapon*>(OtherActor));
	}
}

//Здесь переключатель - Switch. Поскольку здесь проще всего работать с тегами, то вместо Switch мы исп-ем конструкцию else if по тегам
//головы, тела, рук и ног. Мы смотрим, какой из компонентов нашего актора пересекся с оружием.
//Т.е. по факту мы взяли некоторое обобщенное событие оверлапа, и разделили его на понятные DamageHandler конкретные вызовы.
//Если надо - можем добавить какие-то реализации.

//Внимание - мы используем статические константы! Поскольку нам не нужны эти поля где-то в глобальном пространстве.
//Нам не потребуется доступ к ним за пределами этого метода. Объявление их статическими даст нам то, что их инициализация произойдет только
//один единственный раз. В момент 1-го обращения к этому методу. Все остальные разы будет происходить использование уже
//инициализированных переменных. 
inline void UCollisionHandler::SwitchByComponentTag(UPrimitiveComponent* Comp, AWeapon* Weapon)
{
	if(GetDamageHandler()) {
	/** No need expose these static to the global scope since we use them in here only.
	 * Нет необходимости выводить эти статические данные в глобальную область видимости, поскольку мы используем их только здесь.
	*/
		static const FName Head{ "Head"};
		static const FName Body{ "Body"};
		static const FName Arms{ "Arms"};
		static const FName Legs{ "Legs"};
		// Класс UDamageHandler получает оружие на вход и для кажой части тела он будет выполнять свою цепочку действий.
		//Например - если получен урон голове, то мы захотим учесть наличие защиты - например шлема. + Наличие каких-то бафов
		//или дебафов. Положит-х или отрицательных эффектов. С телом, руками или ногами нужна такая же логика.
		//Проверка наличия брони итд.
		if(Comp->ComponentHasTag(Head))
			GetDamageHandler()->DamageHead(Weapon);
		else if(Comp->ComponentHasTag(Body))
			GetDamageHandler()->DamageBody(Weapon);
		else if(Comp->ComponentHasTag(Arms))
			GetDamageHandler()->DamageArms(Weapon);
		else if(Comp->ComponentHasTag(Legs))
			GetDamageHandler()->DamageLegs(Weapon);
	}
}

//В данном контексте применяется концепция адаптера в виде класса UCollisionHandler,
//который выполняет адаптацию между компонентами движка Unreal Engine и функционалом обработки урона (UDamageHandler) и оружия (AWeapon).

// 1. UCollisionHandler является адаптером между компонентами столкновения (UPrimitiveComponent)
// и функционалом обработки урона (UDamageHandler) и оружия (AWeapon).
// С помощью метода SwitchByComponentTag и других методов, UCollisionHandler адаптирует события столкновения
// и обработку урона для использования функционала UDamageHandler и AWeapon.

// 2. В методе GetDamageHandler используется динамическое приведение типа для адаптации компонента UDamageHandler
// из компонентов владельца (owner) класса UCollisionHandler.


// Таким образом, в представленном коде паттерн Адаптер применяется для создания класса,
// который адаптирует интерфейс компонентов столкновения к функционалу обработки урона и оружия.
// Это показывает, как паттерны программирования могут использоваться в совокупности с фреймворками
// и игровыми движками для решения конкретных задач разработки игр.


/*В С++ приведение типов с помощью dynamic_cast является безопасным, если класс, к которому вы хотите привести,
 *имеет иерархию наследования и указатель на базовый класс указывает на объект производного класса.
 *В данном случае, если оба класса AActor и AWeapon имеют иерархию наследования и AWeapon производный класс от AActor,
 *то приведение типа с помощью dynamic_cast будет возможным и безопасным.

Вот пример, который демонстрирует это:

class AActor {
	// ...
};

class AWeapon : public AActor {
	// ...
};

int main() {
	AActor* obj = new AWeapon();
	AWeapon* weapon = dynamic_cast<AWeapon*>(obj); // Приведение типа безопасно, так как obj указывает на объект AWeapon
	if (weapon) {
		// использование объекта weapon
	} else {
		// obj не является экземпляром класса AWeapon
	}
}

Этот код позволяет безопасно привести объект типа AActor к типу AWeapon, потому что на самом деле объект,
на который указывает указатель obj, является объектом класса AWeapon.*/

