
#pragma once

#include "CoreMinimal.h"
#include "MyAIBase.h"

#include "BattleHandleComponent.h"
#include "HealthHandleComponent.h"

#include "MyEnemy.generated.h"

// class UBattleHandleComponent;
// class UHealthHandleComponent;

/*
 * @breaf Основной враг, который может атаковать воображаемого игрока и получать от него урон
 */

UCLASS()
class PATTERNS_API AMyEnemy : public AMyAIBase
{
	GENERATED_BODY()

public:
	AMyEnemy();

private: 
	/*
	 * @breaf Управляет взаимодействием, связанным с боем, таким как атака у игрока.
	 */
	UPROPERTY(meta = (AllowPrivateAccess = "true"), VisibleAnywhere, BlueprintReadOnly, Category= Components)
	UBattleHandleComponent* BattleHandler{nullptr};

	/*
	 * @breaf Обрабатывает события, связанные со здоровьем, таким как получение урона от атаки игрока
	 */
	UPROPERTY(meta = (AllowPrivateAccess = "true"), VisibleAnywhere, BlueprintReadOnly, Category= Components)
	UHealthHandleComponent* HealthHandler{nullptr};
};

inline  AMyEnemy::AMyEnemy()
{
	//Components instantiation
	BattleHandler	= CreateDefaultSubobject<UBattleHandleComponent>(TEXT("BattleHandler"));
	HealthHandler	= CreateDefaultSubobject<UHealthHandleComponent>(TEXT("HealthHandler"));
}
