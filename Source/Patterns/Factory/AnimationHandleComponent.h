// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AnimationHandleComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PATTERNS_API UAnimationHandleComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAnimationHandleComponent();

	UPROPERTY(EditAnywhere, Category = "Animation")
	UAnimInstance* AnimInstance;

	UFUNCTION(BlueprintCallable, Category = "Animation")
	void PlayAnimation(UAnimMontage* AnimationMontage, float PlayRate = 1.0f);

	UFUNCTION(BlueprintCallable, Category = "Animation")
	void StopAnimation(UAnimMontage* AnimationMontage);

	UFUNCTION(BlueprintCallable, Category = "Animation")
	void SetSpeed(UAnimMontage* AnimationMontage, float PlayRate);

	UFUNCTION(BlueprintCallable, Category = "Animation")
	void SetBlendInTime(UAnimMontage* AnimationMontage, float BlendInTime);

	UFUNCTION(BlueprintCallable, Category = "Animation")
	void SetBlendOutTime(UAnimMontage* AnimationMontage, float BlendOutTime);

	UFUNCTION(BlueprintCallable, Category = "Animation")
	void SetNotifyEvent(UAnimMontage* AnimationMontage, FAnimNotifyEvent NotifyEvent);

protected:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


private:
	//Здесь приватные переменные и функции для управления анимациями
	
};
