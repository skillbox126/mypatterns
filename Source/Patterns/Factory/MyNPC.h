
#pragma once

#include "CoreMinimal.h"
#include "MyAIBase.h"
#include "QuestHandleComponent.h"
#include "MyNPC.generated.h"

// class UQuestHandleComponent;

/*
 * @breaf Basic NPC that can give some quests to our imaginary player
 */

UCLASS()
class PATTERNS_API AMyNPC : public AMyAIBase
{
	GENERATED_BODY()

public:
	AMyNPC();

private:
	/* @breaf Allows (Позволяет) this NPC give quests to the player
	 * 
	 */
	UPROPERTY(meta = (AllowPrivateAccess = "true"), VisibleAnywhere, BlueprintReadOnly, Category= Components)
	UQuestHandleComponent* QuestHandler{nullptr};
};

inline  AMyNPC::AMyNPC()
{
	//Components instantiation
	QuestHandler	= CreateDefaultSubobject<UQuestHandleComponent>(TEXT("QuestHahdler"));
}
