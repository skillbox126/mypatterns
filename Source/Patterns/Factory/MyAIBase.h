// Фабричный метод (или виртуальный конструктор). Виртуальный -т.к. ВСЁ основывается на наследовании.

// - Это порождающий паттерн.
// - Определеят Общий интерфейс для создания объектов в суперклассе.
// - Позволяет подклассам Менять ТИП создаваемого объекта.

//Рассматривать будем на примере ИИ.
//Допустим разрабатываем игру - симулятор выполнения квестов, где есть игроки (или 1 игрок). В игре есть NPC(Non plaer character),
//кот-е дают игроку квесты и завершают их.
//И тут вдруг геймдиз дает указание  -добавить врагов, ИИ для управления NPC, которые будут драться с врагом (?), а не только давать ему квесты.
//При этом для NPC было изначально прописано много ф-й. И теперь придется перtписывать большой объем логики. Но -мы видим, что обращения к классу NPC
//можно заменить на обращения к некторому абстрактному суперклассу.
//Для того, чтобы добавить новых врагов или новый тип ИИ - можем применить паттерн Фабричный метод - он позволит создавать объекты не напрямую,
//а через свои подклассы. Соответсвенно, все классы ИИ мы приведем к общему интерфейсу, для того чтобы с ним мог работать клиентский код.
//И все новые классы ИИ мы будем наследовать от этого общего интерфейса.

//Есть Создатель - некий абстрактный класс. Он возвращает объект с общим интерфейсом для всех продуктов. Создатель также объявляет Метод создания
//данного продукта.
//Создатель создает Конкретный продукт определенного подкласса. Он будет отличаться от своего родителя на реализацию интерфейсных ф-й.

//У нас есть создатель AMyAIBase, кот-й оперирует нужным ему подклассом продукта.

//ВАЖНО! В Фабричном методе ВСЁ основывается на наследовании.
//т.е. у нас есть абстрактный продукт из которого получаются конкретные реализации продуктов.
//А также абстрактный создатель, кото-й является родителем конкректных создателей.
//Это является достоинством. Но есть и недостаток! - Придется много создавать наследников!

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "GameFramework/PawnMovementComponent.h"
#include "AnimationHandleComponent.h"
#include "CollisionHandleComponent.h"
#include "InteractionHandleComponent.h"
#include "TaskHandleComponent.h"

#include "MyAIBase.generated.h"

//Forward declaration
//Здесь декларируем все нужные компоненты для работы ИИ  - будут присутсвовать и для NPC и для врагов
// class UAnimationHandleComponent;
// class UCollisionHandleComponent;
// class UInteractionHandleComponent;
// class UPawnMovementComponent;
// class UTaskHandleComponent;

/* Кратко о базовом классе искусственного интеллекта
 * details создает все необходимые базовые компоненты
 * любым потомкам(descendent) искусственного интеллекта для правильной(properly) работы
 */ 

UCLASS()
class PATTERNS_API AMyAIBase : public APawn //класс абстрактного продукта  - наследуемся от Pawn
{
	GENERATED_BODY()

protected:
	AMyAIBase();
	// {
	// 	//здесь В КОНСТРУКТОРЕ мы инстанцируем все необходимые для нашего продукта компоненты
	// 	AnimationHadler		= CreateDefaultSubobject<UAnimationHadleComponent>(TEXT ("AnimationHadler"));
	// 	CollisionHandler	= CreateDefaultSubobject<UCollisionHandleComponent>(TEXT("CollisionHandler"));
	// 	InteractionHandler	= CreateDefaultSubobject<UInteractionHandleComponent>(TEXT("InteractionHandler"));
	// 	MovementHendler		= CreateDefaultSubobject<UPawnMovementComponent>(TEXT("MovementHendler"));
	// 	TaskHandle		= CreateDefaultSubobject<UTaskHandleComponent>(TEXT("TaskHandle"));
	// };

private:
	/*
	 * @bref (коротко) Описание событий и входных данных, которые должны быть переданы экземпляру animation
	 */
	UPROPERTY(meta = (AllowPrivateAccess = "true"), VisibleAnywhere, BlueprintReadOnly, Category= Components)
	UAnimationHandleComponent* AnimationHadler{nullptr};

	/*
	 * @bref Получение коллизий от всех коллайдеров и передает их всем, кто этого ждет
	 */
	UPROPERTY(meta = (AllowPrivateAccess = "true"), VisibleAnywhere, BlueprintReadOnly, Category= Components)
	UCollisionHandleComponent* CollisionHandler{nullptr};

	/*
	 * @bref Управляет всеми взаимодействиями с предметами в мире
	 */
	UPROPERTY(meta = (AllowPrivateAccess = "true"), VisibleAnywhere, BlueprintReadOnly, Category= Components)
	UInteractionHandleComponent* InteractionHandler{nullptr};

	/*
	 * @bref Обрабатывает все вводимые данные о перемещении
	 */
	UPROPERTY(meta = (AllowPrivateAccess = "true"), VisibleAnywhere, BlueprintReadOnly, Category= Components)
	UPawnMovementComponent* MovementHendler{nullptr};

	/*
	 * @bref Мозг искусственного интеллекта в буквальном смысле обрабатывает все возможные действия, которые
	 * может совершить этот экземпляр искусственного интеллекта
	 * //Компонет отвечает за все задачи, стоящие перед ИИ
	 */
	UPROPERTY(meta = (AllowPrivateAccess = "true"), VisibleAnywhere, BlueprintReadOnly, Category= Components)
	UTaskHandleComponent* TaskHandle{nullptr};
};

inline  AMyAIBase::AMyAIBase()
{
	//здесь В КОНСТРУКТОРЕ мы инстанцируем все необходимые для нашего продукта компоненты
	AnimationHadler		= CreateDefaultSubobject<UAnimationHandleComponent>(TEXT ("AnimationHadler"));
	CollisionHandler	= CreateDefaultSubobject<UCollisionHandleComponent>(TEXT("CollisionHandler"));
	InteractionHandler	= CreateDefaultSubobject<UInteractionHandleComponent>(TEXT("InteractionHandler"));
	MovementHendler		= CreateDefaultSubobject<UPawnMovementComponent>(TEXT("MovementHendler"));
	TaskHandle		= CreateDefaultSubobject<UTaskHandleComponent>(TEXT("TaskHandle"));
}

// Фабричный метод

//Плюсы:
// - Избавляет класс от привязки к конкретным создаваем типам
// - Упрощает добавление новых создаваемых объектов
// - Реализуется принцип SRP - у нас каждый создатель имеет одну конкретную роль! И отвечает перед одним клиентским классом клиентской логики.
// - Можем экономить системные ресурсы за счет повторного использования уже созданных объектов. Благодаря тому, чт о создатель будет
//тем или иным способом их кешировать!

//Минусы:
// - Даже для одного объекта потребуется своя фабрика.
// - Код раздувается из-за того, что появляется иерархия паралленых классов. Т.е. будет идти иерархия создателя от суперкласса и от наследника.
//А параллено ему будет идти иерархия продуктов.

//Этот метод можно применять когда заранее известны ТИПЫ и ЗАВИСИМОСТИ объектов. Мы можем построить иерархию наследования.
//Так же можно применять, если мы хотим дать возможность расширять нашу логику другим разработчикам.

//Со временем паттерн Фабричный метод может перерасти в паттерн Абстрактная фабрика.