// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimationHandleComponent.h"


// Sets default values for this component's properties
UAnimationHandleComponent::UAnimationHandleComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UAnimationHandleComponent::PlayAnimation(UAnimMontage *AnimationMontage, float PlayRate)
{
}

void UAnimationHandleComponent::StopAnimation(UAnimMontage *AnimationMontage)
{
}

void UAnimationHandleComponent::SetSpeed(UAnimMontage *AnimationMontage, float PlayRate)
{
}

void UAnimationHandleComponent::SetBlendInTime(UAnimMontage *AnimationMontage, float BlendInTime)
{
}

void UAnimationHandleComponent::SetBlendOutTime(UAnimMontage *AnimationMontage, float BlendOutTime)
{
}

void UAnimationHandleComponent::SetNotifyEvent(UAnimMontage *AnimationMontage, FAnimNotifyEvent NotifyEvent)
{
}


// Called when the game starts
void UAnimationHandleComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UAnimationHandleComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

