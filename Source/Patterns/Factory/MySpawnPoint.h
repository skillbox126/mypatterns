//Клиентский код, который будет упрощаться нашим спавнером. Предположим, что эти SpawnPoint будут расставляться на карте.
//И их трансформ будет передаваться в ф-ии SpawnAT()

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MySpawnerEnemy.h"
#include "MySpawnerNPC.h"
// #include "MySpawnerAI.h"
#include "MySpawnPoint.generated.h"

class AMyAIBase;
class AMySpawnerAI;
//@Note
//Запись #include "MySpawnerAI.h" предполагает, что содержимое файла MySpawnerAI.h будет вставлено в это место при компиляции,
//что позволит использовать определения из этого файла в текущем.

// С другой стороны, запись class AMySpawnerAI; является объявлением класса, без включения его определения.
// Это означает, что в этом файле мы сообщаем компилятору о существовании класса AMySpawnerAI, без предоставления его полного определения.
// Это может использоваться, например, для обозначения, что класс будет определен позже или что он будет использован только по Указателю или Ссылке.


/* @breaf This enum shall match avialable spawner type
 * Это перечисление должно соответствовать доступному типу создателя
 * Мы создаем какой-то подкласс ИИ и такое же поле добавляем в пречисление типов ИИ для того чтобы они были сопоставимы
 * и мы не работали с голыми строками
 */
UENUM(BlueprintType)
enum class ETypeOfAI : uint8
{
	EToAI_Enemy	= 0 UMETA(DisplayName = "Enemy"), //перечесляем через запятую!!!
	EToAI_NPC	= 1 UMETA(DisplayName = "NPC")
};

/* @breaf An Actor to be placed on the level as a point from which we'll get spawn AI type and transform info.
 * SpawnPoint помещается на уровень в качестве точки, из которой мы получим тип AI и transform info и будем передавать их в SpawnAT().
 */
UCLASS()
class PATTERNS_API AMySpawnPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMySpawnPoint();

	/* @breaf Spawn AI at given transform
	 */
	UFUNCTION(BlueprintCallable, Category= AI)
	virtual AMyAIBase* SpawnAI_BP();

	// virtual AMyAIBase* SpawnAT(const FTransform &SpawnTransform) override;
	
private:
	/* @breaf For iach instance of this actor we'll select its own AI type
	 * Для каждого экземпляра этого актера мы выберем его собственный тип искусственного интеллекта
	 */
	UPROPERTY(meta = (AllowPrivateAccess= "true"), BlueprintReadWrite, EditInstanceOnly, Category= AI)
	ETypeOfAI PointType{ETypeOfAI::EToAI_NPC}; //делаем PointType по дефолту NPC

	/* @breaf We'll store local reference to a spawner for easy access
	 * Мы сохраним локальную ссылку на источник для удобства доступа
	 */
	UPROPERTY(meta = (AllowPrivateAccess= "true"), BlueprintReadWrite, EditInstanceOnly, Category= AI)
	AMyAIBase* Spawner{nullptr}; //По дефолту Spawner инициализирован нулевым указателем.
};

//шаблонная ф-я для спавна /шаблон класса типа TAIClass
//Функция возвращает указатель на объект класса AMyAIBase.
template <class TAIClass> 
static AMyAIBase* GetSpawner()
{
	//Цикл for начинается с инициализации итератора It. Затем он проверяет, что значение итератора является допустимым (It),
	//и, если это так, выполняет тело цикла. После этого приращивает итератор (++It), чтобы перейти к следующему элементу.
	for (TObjectIterator<TAIClass> It; It; ++It)
	{
	return (*It); //возвращмем разыменованный
	}
	return nullptr;
};

inline  AMySpawnPoint::AMySpawnPoint()
{
	//Setting up our spawner
	if(!Spawner)
	{
		switch (PointType)
		{
		case ETypeOfAI::EToAI_Enemy:
			Spawner = GetSpawner<UMySpawnerEnemy>();
			break;
		case ETypeOfAI::EToAI_NPC:
			Spawner = GetSpawner<UMySpawnerNPC>();
			break;
		default:
			//Since we shell not ever end up in here - Поскольку мы никогда здесь не окажемся
			// ASSUME(0); //В языке C++ команда ASSUME обычно используется для указания компилятору, что определенное условие
			// должно быть верным во время выполнения программы. Если условие не выполняется, программа может завершиться аварийно
			// или выполняться некорректно. ASSUME(0) обозначает, что условие должно быть ложным.
			// ASSERT(0);
			__assume(0); //нужно заменить ASSUME на __assume для моего компиллятора
			UE_LOG(LogTemp,Error,TEXT("AMySpawnPoint::AMySpawnPoint() - Call Default"));
		}
	}
}

//Здесь возврат указателя на класс, AMyAIBase, но не используется возврат класса ни в одном случае???
inline AMyAIBase* AMySpawnPoint::SpawnAI_BP()
{
	AMyAIBase* ReturnedActor = Cast<AMyAIBase>(Spawner); //используем только Spawner
	return ReturnedActor ? ReturnedActor : nullptr;
	//Каким образом заспавнить объект?
	// вариант а) точки уже расставлены и каждая имеет координаты (трансформу)
	// вариант б) при спавне используется координата спавнера (тогда смысл точки пропадает, можно заменить на число и в цикле спавнить ОТ и ДО числа)
	// вариант в) спавнить на начальном этапе точки, затем брать их трансформу

	//Тут запутался совсем и залез в дебри. Особенно с вашей подсказкой((
	//1) Само по себе приведение типов AMyAIBase* ReturnedActor = Cast<AMyAIBase>(Spawner); здесь беcсмысмысленно же?
	//Хотя, я кажется вас понял - вы просто хотите мне показать, что что должен произойти либо возврат класса либо ничего, т.е.
	//nullptr  - нулевой указатель? Не 0 - тут конечно ошибка.
	//Но дело не в этом, а сомневался в самой необходимости явного возврата в принципе. Ведь Компиллятор ПРЕДУПРЕЖДАЕТ, что хотелось бы явный возврат,
	//но, получается, допускает и такой синтаксис, как  в уроке:
	 // if(Spawner)
	 // 	AMyAIBase* UMySpawnerAI::SpawnAT();

	//Т.е. В данном случае, если Spawner равен nullptr, то функция SpawnAI_BP() автоматически вернет nullptr.
	//Это происходит из-за того, что в C++ при обращении к объекту по нулевому указателю будет возвращено значение nullptr.

	// Если Spawner не равен nullptr, то будет вызван метод SpawnAT() объекта Spawner, который создаст новый объект AMyAIBase* и вернет его.
	
	// Таким образом, данная функция возвращает либо объект типа AMyAIBase*, если Spawner не равно nullptr, либо nullptr, если Spawner равно nullptr.

	//А если я всё-таки явно хочу сделать return!
	// if (Spawner)
	// {
	// 	UMySpawnerAI* SpawnerAI = NewObject<UMySpawnerAI>(); // создаем объект класса UMySpawnerAI
	// 	if (SpawnerAI)
	// 	{
	// 		//В этом случае мне явно надо определить координаты спавна! Но координаты у меня определяются, когда я ставлю точку на уровень.
	// 		//Тогда надо сюда передавать координаты спавна, а не нули..
	// 		FTransform SpawnTransform = {0.f, 0, 0}; //И здесь надо заполнить SpawnTransform соответствующими данными
	// 		return SpawnerAI->SpawnAT(SpawnTransform);
	// 	}
	// }
	// return nullptr;
}



//Макрос _ASSERT (- Утвержать)позволяет вставить в отладочную версию программы
//различные проверки того, насколько правильно она выполняется. В release версии все они будут удалены.
//
//ASSUME (- Предполагать)
// При компиляции версии для конечного пользователя, в отличие от обычного ASSERT,
// котрорый просто исключается, новый макрос будет заменён специальным ключевым словом __assume.

// Назначение его состоит в том, чтобы сообщить дополнительные сведения для системы оптимизации компилятора,
// учитывая которые он сможет создать более оптимальный ассемблерный код. 
// Выражение внутри __assume должно быть всегда истино. 
// Если же оно всегда ложно, значит данное место в программе никогда не исполняется. 