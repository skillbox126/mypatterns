// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MySpawnerAI.h"
#include "MyNPC.h"
#include "MySpawnerNPC.generated.h"

/** @breaf This spawner knows how to create NPCs only
 */
UCLASS()
class PATTERNS_API UMySpawnerNPC : public UMySpawnerAI
{
	GENERATED_BODY()
	
public:
	virtual AMyAIBase* SpawnAT(const FTransform &SpawnTransform) override;
};

inline AMyAIBase* UMySpawnerNPC::SpawnAT(const FTransform &SpawnTransform)
{
	return dynamic_cast<AMyAIBase*>(GetWorld()->SpawnActor(AMyNPC::StaticClass(), &SpawnTransform));
}
