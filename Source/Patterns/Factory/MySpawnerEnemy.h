
#pragma once

#include "CoreMinimal.h"
#include "MySpawnerAI.h"
#include "MyEnemy.h"
#include "MySpawnerEnemy.generated.h"

class MyEnemy;

/** @breaf This spawnr knows how to create enemies only
 * 
 */
UCLASS()
class PATTERNS_API UMySpawnerEnemy : public UMySpawnerAI
{
	GENERATED_BODY()
	
public:
	//перегружаем метод SpawnAT() 
	virtual AMyAIBase* SpawnAT(const FTransform &SpawnTransform) override;
};

inline AMyAIBase* UMySpawnerEnemy::SpawnAT(const FTransform &SpawnTransform)
{
	// и создаем врага в переданном ему трансформе
	return dynamic_cast<AMyAIBase*>(GetWorld()->SpawnActor(AMyEnemy::StaticClass(), &SpawnTransform));
	//для этого приводим класс AMyEnemy к указателю на AMyAIBase и возвращаем его. 
}
//Получется, что все наши клиенты общаются с абстрактным суперклассом и имеют общий интерфейс.