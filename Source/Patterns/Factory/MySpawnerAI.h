// Абстрактный класс UMySpawnerAI может инстанциироваться в GameInstance чтобы быть только в одном экземпляре.
//Либо можно создать его во множестве семплеров( Sample - образец), в зависимости от потребностей.
//Важно!
//Логика создания не всегда является ключевой и единственной для некото-го абстрактного создателя и его наследников.
//Т.е. у этого класса м.б. бизнес-логика и помимо этого он может содержать абстрактную ф-ю (например SpawnAT()),
//либо какую-то базовую ф-ю создания нек-х объектов. В нашем примере - только одна ф-я: генрировать объекты типа AMyAIBase.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "MySpawnerAI.generated.h"

class AMyAIBase;
class UClass;
class UWorld;

/** @breaf A base AI spawner
 *  @details We assume(предполагаем) that it world is instantiated in custom (пользовательской) game
 *   wich whill also be set as in outer (как внешний) for this object to get a valid world
 */
UCLASS(Blueprintable, BlueprintType)
class PATTERNS_API UMySpawnerAI : public UObject
{
	GENERATED_BODY()
	
public:
	/** @breaf Return up-to-date (актуальный) world instanse (экземпляр)
	 *   if only fetch (извлечь, выборка) on
	 *   Чтобы спавнить акторов в Мире - сначала нужно создать реальный Мир.
	 */
	UFUNCTION(BlueprintCallable)
	virtual UWorld* GetWorld() const override;//Объявляю, но не описываю - ниже

	/** @breaf Spawn a descendent(потомок) of AMyAIBase at given location
	 * Здесь главное! Связь между классами! Ф-я SpawnAT() является потомком AMyAIBase.
	 */
	virtual AMyAIBase* SpawnAT(const FTransform& SpawnTransform) PURE_VIRTUAL(UMySpawnerAI::SpawnAT, return nullptr; );
};

//А здесь описание ф-ии GetWorld()
inline UWorld* UMySpawnerAI::GetWorld() const
{
	UWorld* World{nullptr};
	if(GetOuter()) //Возвращает UObject, в котором находится этот объект
		World = GetOuter()->GetWorld();
	
	/** @TIP: The less returns per metod we use, the easier it is for compiler to optimise
	 * СОВЕТ: Чем меньше возвращаемых значений для каждого метода, который мы используем, тем легче компилятору оптимизировать
	 */
	return World;
}

//Функция компилятора UE4, стандарт C ++, не имеет этой функции.

//Функция PURE_VIRTUAL: UClass, унаследованный от UObject, должен иметь возможность создания экземпляра,
//система должна иметь возможность вызывать конструктор по умолчанию, так называемый CDO (Class Default Object), так что он не может
//содержать чисто виртуальные функции, но в определенных конкретных сценариях его виртуальные функции должны быть реализованы
//в его подклассах (а не в дополнительных реализациях обычных виртуальных функций),
//тогда вы можете использовать PURE_VIRTUAL, поэтому его cpp-файл не нуждается в реализации этой функции,
//и в то же время вы можете напрямую вызывать эту функцию в других функциях, а подклассы должны применять эту функцию

//использование:

/* With PURE_VIRTUAL we skip implementing the function in SWeapon.cpp and can do this in SWeaponInstant.cpp / SFlashlight.cpp instead */
//virtual void FireWeapon() PURE_VIRTUAL(ASWeapon::FireWeapon, );