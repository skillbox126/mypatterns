//MyGameQuestImplementationNew.h:
// - Этот файл объявляет класс UMyGameQuestImplementationNew, который представляет конкретную реализацию квеста для игры.
// - Он наследуется от IMyQuestImplementation, что обеспечивает реализацию методов интерфейса.
#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "MyQuestImplementation.h"
#include "MyGameQuestImplementationNew.generated.h"

/**
 * 
 */
UCLASS()
class PATTERNS_API UMyGameQuestImplementationNew : public UObject, public IMyQuestImplementation
{
	GENERATED_BODY()
public:
	void CreateQuest() override;
	void UpdateQuest() override;
	void CompleteQuest() override;
};
