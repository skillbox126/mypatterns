// Паттерн Легковес. Структрурный паттерн.

//В данном примере мы создали интерфейс MyQuestImplementation, абстрактный класс MyQuestNew
//и конкретную реализацию MyGameQuestImplementationNew.

//В классе MyQuestNew мы используем объект типа MyQuestImplementation для выполнения методов, 
//cвязанных с созданием, обновлением и завершением квестов.
//
//В результате мы получаем отдельные объекты для абстракции и реализации, что соответствует паттерну Легковес.

// MyQuestNew.h:
//    - Этот файл объявляет класс UMyQuestNew, который представляет абстракцию для квеста.
//    - В этом классе определены методы Create(), Update() и Complete(), которые будут использоваться для работы с квестами.
//    - Также определен метод SetImplementation(), который устанавливает конкретную реализацию квеста.
#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "MyQuestImplementation.h"
#include "MyQuestNew.generated.h"

/**
 * 
 */
UCLASS()
class PATTERNS_API UMyQuestNew : public UObject
{
	GENERATED_BODY()
public:
	void Create();
	void Update();
	void Complete();
	//конкретная реализация квеста.
	void SetImplementation(IMyQuestImplementation* InImplementation);

private:
	//ссылка на легкий объект типа IMyQuestImplementation
	IMyQuestImplementation* Implementation;
	
};

/*

Как происходит реализация паттерна Легковес в данном примере:
- Паттерн Легковес предполагает разделение класса на тяжелые и легкие объекты.
Легкие объекты хранят общие данные, в то время как тяжелые объекты хранят уникальные данные.
- В нашем примере интерфейс IMyQuestImplementation выступает в роли легкого объекта, поскольку он содержит общие методы для всех реализаций квестов.
- Класс UMyQuestNew играет роль контекста и хранит ссылку на легкий объект типа IMyQuestImplementation.
- Реализации квестов, такие как UMyGameQuestImplementationNew, представляют собой тяжелые объекты,
хранящие в себе уникальные данные и реализующие методы интерфейса.
- При помощи паттерна Легковес мы можем использовать один и тот же легкий объект IMyQuestImplementation для различных тяжелых реализаций квестов,
сохраняя таким образом память и повышая эффективность программы.

Таким образом, реализация паттерна Легковес в данном примере позволяет абстрагировать общую функциональность квестов
от их конкретных реализаций и использовать их эффективно в рамках системы игры.
*/

// В терминологии паттернов проектирования и принципов SOLID, классический паттерн "Легковес" (Flyweight) используется
// для уменьшения использования памяти за счет разделения внутреннего и внешнего состояния объектов.
//
// - Внутреннее состояние - это информация, которая является общей для множества объектов легковеса и может быть разделена между ними.
// Это часто называемая "разделяемая" информация. Например, в примере с заданием это может быть информация о самом задании, его типе, описании и т. д.
//
// - Внешнее состояние - это информация, которая уникальна для каждого объекта легковеса.
// Этот уникальный набор данных содержится вне легковеса и применяется для настройки его поведения.
// В контексте задания это могут быть данные о состоянии выполнения задания,
// результат выполнения, или другие специфические данные, связанные с конкретной задачей.
// При использовании паттерна "Легковес" важно аккуратно разделять внутреннее и внешнее состояние,
// чтобы обеспечить эффективное использование памяти и возможность повторного использования общих данных между объектами.

//У меня для квестов:
// Есть легковес для представления различных типов квестов в игровом приложении,
//  - внутреннее состояние может содержать общую информацию о типе квеста, его наградах и т. д.,
//  - внешнее состояние может содержать уникальные данные для каждого квеста, например, прогресс его выполнения, текущее состояние и т. д.

