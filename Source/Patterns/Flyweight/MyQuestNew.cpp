//MyQuestNew.cpp:
// - Этот файл содержит реализацию методов класса MyQuestNew.
// - Методы Create(), Update() и Complete() делегируют выполнение соответствующих методов реализации квеста через объект Implementation.

#include "MyQuestNew.h"
void UMyQuestNew::Create()
{
  if(Implementation)
  {
    Implementation->CreateQuest();
  }
}

void UMyQuestNew::Update()
{
  if (Implementation)
  {
    Implementation->UpdateQuest();
  }
}

void UMyQuestNew::Complete()
{
  if (Implementation)
  {
    Implementation->CompleteQuest();
  }
}

void UMyQuestNew::SetImplementation(IMyQuestImplementation* InImplementation)
{
  Implementation = InImplementation;
}





