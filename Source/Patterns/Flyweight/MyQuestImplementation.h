// MyQuestImplementation.h:
// - Этот файл содержит объявление интерфейса UQuestImplementation Interface с помощью макроса UINTERFACE().
// Этот макрос указывает, что данный класс является интерфейсом для Unreal Engine.
// - Далее определяется сам интерфейс IMyQuestImplementation,
// который содержит чисто виртуальные методы CreateQuest(), UpdateQuest() и CompleteQuest().
#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MyQuestImplementation.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UMyQuestImplementation : public UInterface
{
	GENERATED_BODY()
};

class PATTERNS_API IMyQuestImplementation
{
	GENERATED_BODY()
public:
	virtual void CreateQuest() = 0;
	virtual void UpdateQuest() = 0;
	virtual void CompleteQuest() = 0;
};
