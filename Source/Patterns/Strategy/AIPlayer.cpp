
#include "AIPlayer.h"

//   	//  - В методе SetStrategy можно задать новую стратегию для AIPlayer.
void UAIPlayer::SetStrategy(IStrategy* NewStrategy)
{
  CurrentStrategy = NewStrategy;
}

//   - Метод DoAction вызывает метод ExecuteStrategy текущей стратегии.
void UAIPlayer::DoAction()
{
  if (CurrentStrategy)
  {
    CurrentStrategy->ExecuteStrategy();
  }
}

