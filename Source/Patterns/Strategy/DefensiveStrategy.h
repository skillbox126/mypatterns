// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IStrategy.h"

class PATTERNS_API DefensiveStrategy : public IStrategy
{
public:
	virtual void ExecuteStrategy() override;
};
