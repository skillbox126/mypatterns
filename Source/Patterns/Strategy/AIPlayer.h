//создаем класс AIPlayer, который представляет искусственный интеллект для вражеских персонажей.
//Этот класс имеет возможность смены стратегии поведения в зависимости от текущей ситуации.

//- Класс AIPlayer наследуется от UObject и содержит указатель на текущий объект стратегии.

#pragma once

#include "CoreMinimal.h"
#include "CoreMinimal.h"
#include "IStrategy.h"

class UAIPlayer : public UObject
{
public:
   	//  - В методе SetStrategy можно задать новую стратегию для AIPlayer.
	void SetStrategy(IStrategy* NewStrategy);

	//   - Метод DoAction вызывает метод ExecuteStrategy текущей стратегии.
	void DoAction();

private:
	//указатель на текущий объект стратегии.
	IStrategy* CurrentStrategy;
};

