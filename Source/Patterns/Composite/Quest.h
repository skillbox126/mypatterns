//UQuest - это наследник UComp_Task.
//Он содержит в себе указатели на возможные подтаски.
//И Quest будет считаться выполненным, когда все подтаски нашего Quest-а будут выполнены. 


#pragma once

#include "CoreMinimal.h"
#include "Comp_Task.h"
#include "Quest.generated.h"

UCLASS()
class PATTERNS_API UQuest : public UComp_Task
{
	GENERATED_BODY()
public:
	//Здесь содержится интерфейс, кот-й мы определили в UComp_Task и теперь мы его пергружаем в Quest, чтобы добавить уникальное поведение.
	//Когда наше состояние зависит не только от нас самих, но и от подобъектов, на которое мы ссылаемся.
	
	/** Begin: UComp_Task interface ovveride. We ovveride parent methods since (так как)  we now have subtasks. 
	* Мы переопределяем родительские методы, поскольку теперь у нас есть подзадачи.
	*/
	virtual bool GetIsTaken() const override;
	virtual bool GetIsFinishd() const override;
	virtual void Take() override;
	virtual void Finish() override;
	/** End: UComp_Task interface ovveride. 
	*/

	//Так же здесь появляется метод AddTask() кот-я позволяет добавить таск или его наследников в коллекцию Tasks.
	//Тем самым мы будем стороить дерево заданий.
	/** @brief Add a task into chane of execution. Добавьте задачу в режим изменения выполнения.
	*/
	void AddTask(UComp_Task* Task);
	
private:
	/** @brief All the dependended tasks witch need  to be executed for the quest to be finished.
	*  Все зависимые задания, которые необходимо выполнить, чтобы квест был завершен.
	*/
	UPROPERTY()
	TArray<UComp_Task*>Tasks; //массив указателей!!!
};

//Посмотрим для сравнения методы GetIsTaken() в UComp_Task и GetIsFinishd()  - они начинались как простые методы,
//возвращающие значения внутри булевых переменных.
//А в переопределенной версии Quest они уже содержат коллекцию тасков.
//И в данном случае мы считаем, что если хотя бы один из тасков коллекции был взят, то весь квест считался взятым.
//Таск обходит только Часть своих наследников - т.к. при 1-м true обход дерева останавливается и экономятся ресурсы.
//Это + к оптимизации, особенно в вопросе дерева квестов. 

inline bool UQuest::GetIsTaken() const{
	//If ani task is taken, the whole quest is. - Если какое-либо задание выполнено, то выполняется весь квест целиком.
	for(auto i : Tasks) {
		//Прим. i - это итератор, по сути  Task -задача, которую вытаскиваем из массива задач в цикле
		if(i->GetIsTaken()) 
			return true;
	}
	return false;
}

//Здесь таск считается завершенным, Если завершены все таски нашего дерева! Для этого они все должны вернуть 1.
//Если хотя бы один из них вернет 0, То квест не будет завершен, таск не завершен и будем ждать его завершения дальше.
//Опять же, мы идем по дереву ниже и вызываем GetIsFinishd() и умножаем их на исходное значение. Аккумулируя рез-т.
inline bool UQuest::GetIsFinishd() const{
	//If we have no tasks, consider is done. Если у нас нет задач, считайте, что все сделано.
	bool bResult{ true };
	//If all are done, we're done too. Если все сделано, то и мы закончили.
	for(auto Task : Tasks) {
		bResult *= Task->GetIsFinishd(); //умножить равно 1*1*0 -придумано прикольно)
	}
	return bResult;
}

//Здесь в переопределенном методе мы ищем 1-й таск, кот-й еще не взят и возвращаем его. Если этот таск находится в левой
//половине дерева, то мы пропускаем проход по целой половине дерева, что экономит ресурсы. 
inline void UQuest::Take(){
	// Take the first non taken. Возьмите первое незанятое.
	for(auto Task : Tasks) {
		if(!Task->GetIsTaken()) //return bool bIsTaken{ false };
			return Task->Take(); // bIsTaken = true;
	}
}

//Рассмотри механику быстрого завершения таска. Напимер за донат или временную механику для отладки. Чтобы мы могли завершить таск
//одной коммандой. Для таких случев мы вызовем Finish() кот-й завершит выполнение тасков для всего дерева.
//К финишу также приведет и закрытие последнего таска.

inline void UQuest::Finish(){
	/** @Note: No super-class cool is here, poor implementation may violate OCP and LSP.
	 * Здесь нет суперкласса cool, плохая реализация может нарушать OCP и LSP.
	 */
	for(auto Task : Tasks) {
		/** @Note: Jast finish it not to iterate throght whole (весь) tree twice.
	 * Просто завершите его, чтобы не перебирать все дерево дважды.
	 */
		Task->Finish(); 
	}
}

//Здесь метод добавляет таск в коллекцию задач.
inline void UQuest::AddTask(UComp_Task* Task){
	Tasks.Push(Task);
}

