//UGameQuestImplementationNew представляет конкретную реализацию методов интерфейса IIQuestImplementation.
//В этом классе реализованы методы Create, Update и Complete, которые выполнены специфически для игровой реализации квестов.

// Этот класс используется вместе с абстракцией UQuestNew, для предоставления игровой функциональности для квестов,
// при этом отделяет реализацию от абстракции в соответствии с паттерном Мост.
#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "IQuestImplementation.h"
#include "GameQuestImplementationNew.generated.h"

/**
 * 
 */
UCLASS()
class PATTERNS_API UGameQuestImplementationNew : public UObject, public IIQuestImplementation
{
	GENERATED_BODY()
	
public:
	virtual void Create() override;
	virtual void Update() override;
	virtual void Complete() override;
	virtual void CompleteNew() override;

};
