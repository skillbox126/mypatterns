//IIQuestImplementation - это интерфейс, который определяет абстракцию для реализации класса UQuestNew.
//Он определяет методы Create(), Update(), и Complete(), которые представляют действия, которые должен выполнить класс UQuestNew.

//IIQuestImplementation определяет семейство классов реализации.
// - может включать несколько конкретных реализаций, каждая из которых предоставляет различную реализацию методов интерфейса.


#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "IQuestImplementation.generated.h"

class UQuestNew;

UINTERFACE(MinimalAPI)
class UIQuestImplementation : public UInterface
{
	GENERATED_BODY()
};


class PATTERNS_API IIQuestImplementation
{
	GENERATED_BODY()
public:
	virtual void Create() = 0;
	virtual void Update() = 0;
	virtual void Complete() = 0;
	virtual void CompleteNew() = 0;

};
