// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestNew.h"
#include "IQuestImplementation.h"

void UQuestNew::Create(){
  if(Implementation)
    Implementation->Create();
}
void UQuestNew::Update(){
  if(Implementation)
    Implementation->Update();
}
void UQuestNew::Complete(){
  if(Implementation)
    Implementation->Complete();
}

void UQuestNew::CreateNew()
{
  //не задаем определение в интерфейсе  -расширение абстракции
}