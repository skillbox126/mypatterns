//Mage (Наследник GameCharacter):
// - Создан класс AMage, наследуемый от AGameCharacter.
// - Переопределены методы Attack(), Defend() и UseAbility() с конкретной реализацией для мага.

#pragma once

#include "CoreMinimal.h"
#include "GameCharacter.h"
#include "Mage.generated.h"

UCLASS()
class PATTERNS_API AMage : public AGameCharacter
{
	GENERATED_BODY()
	
public:	
	AMage();

	virtual void Attack() override;
	virtual void Defend() override;
	virtual void UseAbility() override;
};
