// Fill out your copyright notice in the Description page of Project Settings.


#include "Warrior.h"

AWarrior::AWarrior()
{
}

void AWarrior::Attack()
{
	// Specific implementation for Warrior's Attack
}

void AWarrior::Defend()
{
	// Specific implementation for Warrior's Defend
}

void AWarrior::UseAbility()
{
	// Specific implementation for Warrior's Ability
}


