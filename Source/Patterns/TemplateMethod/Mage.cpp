// Fill out your copyright notice in the Description page of Project Settings.


#include "Mage.h"

AMage::AMage()
{
}

void AMage::Attack()
{
	// Specific implementation for Mage's Attack
}

void AMage::Defend()
{
	// Specific implementation for Mage's Defend
}

void AMage::UseAbility()
{
	// Specific implementation for Mage's Ability
}

