//Warrior (Наследник GameCharacter):
// - Создан класс AWarrior, который наследуется от AGameCharacter.
// - Переопределены методы Attack(), Defend() и UseAbility() с конкретной реализацией для воина.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameCharacter.h"
#include "Warrior.generated.h"


UCLASS()
class PATTERNS_API AWarrior : public  AGameCharacter

{
	GENERATED_BODY()
	
public:	
	AWarrior();

	virtual void Attack() override;
	virtual void Defend() override;
	virtual void UseAbility() override;
	
};
