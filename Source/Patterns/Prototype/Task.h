// Если у нас есть n-е кол-во NPC, врагов или просто нейтральных персоналий, то мы можем столкнуться с ситуацией,
//когда NPC в рамках своего дерева поведения будут выполнять одни и те же задачи - либо задачи с одинаковыми параметрами.
//В таком случае мы можем создать копию такой задачи с помощью рассмотренного ранее интерфейса.
//И копию задачи мы передадим для работы NPC. 
#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "CanClone.h"
#include "Task.generated.h"

/** @brief A base class for all AI tasks.
 *  @details Shall represent a short action as a part of same greater plan. -
 *  Этот класс задачи  - Должен представлять собой краткое действие
 *  как часть более масштабного плана в дереве поведений.
 */
UCLASS(Blueprintable, BlueprintType)
class PATTERNS_API UTask : public UObject, public ICanClone
{
	GENERATED_BODY()
	/** BEGIN: ICanClone interface implementation. */
	virtual UTask* Clone_BP() override;
	//** END: ICanClone interface implementation. */
	
	float GetDuration_BP() const {
		return Duration;
	};

	bool GetIsRepeatable_BP() const {
		return IsRepeatable;
	};

private:
	/** @brief How long hang completing this task take. - Сколько времени займет выполнение этой задачи?
	*  @details When set via BP editor is clamped to ten minutes. The shorter task is, the better.
	*  При настройке через редактор BP время ограничено десятью минутами. Чем короче задание, тем лучше.
	*/
	UPROPERTY(meta = (AllowPrivateAccess = "true", //Если класс или переменная в 
		ClampMin = 0, ClampMax = 600, //- ограничиваем 10 мин.
		UIMin = 0, UIMax = 600), //ограничиваем для ползунка
		EditAnywhere, BlueprintReadWrite, Category= Task)
	float Duration{ 0.f}; //отвечает за длительность задачи
	
	/** @brief Can this task restarted once done. Можно ли перезапустить эту задачу после ее выполнения?
	*/
	UPROPERTY(meta=(AllowPrivateAccess = true), EditAnywhere, BlueprintReadWrite, Category= Task)
	bool IsRepeatable{ true }; //отвечает за повторяемость задачи
	//Т.Е. ответ на вопрос -можем ли мы по завершении задачи выполнить ее снова
};

//Реализация метода Clone_BP().
//В классической реализации нам бы потребовалось создавать конструктор копий, который бы принимал копируемый объект на вход.
//Но Unreal ограничивает функционал. И перенаправляет в использхование дефолтного конструктора при вызове NewObject.
//И уже из-за этого нам приходится отложить инициализацию полей. Получается, что сначала мы создаем объект
//и только после этого мы присваиваем ему значение копируемого объекта. Это стоит иметь ввиду при данной реализации.

//Если часть логики завязана на конструктор и на то, что в нем будут валидные поля, то в строках с Copy-> этот способ не поможет и не сработает!

inline UTask* UTask::Clone_BP(){
	auto Copy = NewObject<UTask>(GetOuter());
	if(IsValid(Copy)) {
		Copy->Duration = GetDuration_BP();
		Copy->IsRepeatable = GetIsRepeatable_BP();
	}
	return Copy; //класс возвращает созданный объект, в который внесены значения полей копируемого 1-го объекта.
	//Использовать этот класс будет TaskManager

	//Функция UTask::Clone_BP объявлена как возвращающая void* по той причине, что в C++ нельзя вернуть объект по значению,
	//если его тип не известен на момент компиляции (то есть неизвестен до запуска программы).
	//Это связано с требованиями стандарта C++, который не позволяет возвращать объекты конкретного типа, если тип неизвестен на этапе компиляции.

	// Таким образом, чтобы избежать этой проблемы, функция возвращает указатель типа void*,
	// который позволяет представить адрес объекта в памяти без указания конкретного типа.
	// Позднее, этот указатель может быть приведен к нужному типу после его использования.
	// В данном случае, объект Copy возвращается как указатель типа void*.
}
