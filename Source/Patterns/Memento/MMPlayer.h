//В классе AMMPlayer добавим методы для создания и применения мементо:

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Memento.h"
#include "MMPlayer.generated.h"

UCLASS()
class PATTERNS_API AMMPlayer : public AActor
{
	GENERATED_BODY()
	
public:
	AMMPlayer();

	void Move(FVector NewPosition);
	void TakeDamage(int Damage);
	UMemento* CreateMemento() const;
	void RestoreMemento(UMemento* Memento);
    
private:
	FVector Position;
	int Health;
};
