// Fill out your copyright notice in the Description page of Project Settings.


#include "MMPlayer.h"

AMMPlayer::AMMPlayer()
    : Position(FVector::ZeroVector), Health(100)
{
}

void AMMPlayer::Move(FVector NewPosition)
{
	Position = NewPosition;
}

void AMMPlayer::TakeDamage(int Damage)
{
	Health -= Damage;
}

//Создаем снимок позиции игрока и его здоровья
UMemento* AMMPlayer::CreateMemento() const
{
	UObject* Owner = FindObject<UObject>(ANY_PACKAGE, TEXT("YourPlayerObjectName"));
	
	UMemento* NewMemento = NewObject<UMemento>(Owner, UMemento::StaticClass(), FName("PlayerMemento"), RF_NoFlags, nullptr, false, nullptr);
	
	if (Owner && NewMemento != nullptr)
	{
		NewMemento->SetPosition(Position);
		NewMemento->SetHealth(Health);
	}

	return NewMemento;
}

//Восстанавливаем позиции игрока
void AMMPlayer::RestoreMemento(UMemento* Memento)
{
	Position = Memento->GetPlayerPosition();
	Health = Memento->GetPlayerHealth();
}
