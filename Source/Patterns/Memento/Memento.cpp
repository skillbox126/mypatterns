// Fill out your copyright notice in the Description page of Project Settings.


#include "Memento.h"

UMemento::UMemento(FVector PlayerPosition, int PlayerHealth)
    : PlayerPosition(PlayerPosition), PlayerHealth(PlayerHealth)
{
}

FVector UMemento::GetPlayerPosition() const
{
  return PlayerPosition;
}

int UMemento::GetPlayerHealth() const
{
  return PlayerHealth;
}

void UMemento::SetPosition(const FVector &NewPosition)
{
    PlayerPosition = NewPosition;
}

void UMemento::SetHealth(const int NewPlayerHealth)
{
  PlayerHealth = NewPlayerHealth;
}





