// Fill out your copyright notice in the Description page of Project Settings.


#include "InputHandlingComponent.h"


/*
// Sets default values for this component's properties
UInputHandlingComponent::UInputHandlingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInputHandlingComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UInputHandlingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
*/

void UInputHandlingComponent::LockPlayerInput()
{
}

void UInputHandlingComponent::UnlockPlayerInput()
{
}