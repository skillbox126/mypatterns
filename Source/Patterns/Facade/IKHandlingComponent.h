// Компент Инверсной Кинематики
//Для инверсной кинематики мы имеем инфу о скелете

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "IKHandlingComponent.generated.h"

class USkeletalMeshSocket;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PATTERNS_API UIKHandlingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	/** @return This actor's interaction socket. Актор взаимодействия с сокетом
	*/
	//Здесь мы можем получить тот самый сокет, с которым мы хотим взаимодействоать.
	//Внутри скобок могли бы быть какие-то аргументы -показывают какой именно Сокет нужно вернуть.
	USkeletalMeshSocket* GetInteractionSocket() const;

	/** @return Use given socket for IK transform. Используем данный сокет для преобразования IK.
	*/
	//Здесь мы указываем целевой Сокет, с кот-м будет происходить взаимодействие. Сокет другого Актора.
	void UseSocketTransformBegin(USkeletalMeshSocket* TargetSocket);

	/** @return Give up using given socket. Откажитесь от использования данного сокета.
	*/
	//Здесь мы указываем, что именно с этим Сокетом мы больше не хотим взаимодействовать.
	//И возвращаем наши кости в исходное состояние.
	void UseSocketTransformEnd(USkeletalMeshSocket* TargetSocket);
};

//В указанной ситуации мы можем иметь больше связей и объектов, к которым мы обращаемся.
//Допустим -мы можем проигрывать особую анимацию или вызвать какой-то интерфейс на экране -например подсказки -как управлять объектом
//в руке в момент взаимодействия. Покрутить или еще что-то сделать.
//Казалось бы - простое действие содержит в себе целый ряд зависимотстей, которые надо учесть.

//Применимость паттерна
//1-й случай - когда надо предоставить простой интерфейс чтобы клиентский код не испытывал трудности с быстрым вызовом нужной ему логики.
//Также - если при написании кода становится понятным, что некот-е действия начинают содержать сложные цепочки вызовов,
//появляются даже какие-то слои логики, когда одна цепочка должна совершить свою последовательность вызовов, потом стартует
//другая цепочка и все это необходимо для того, чтобы вызвался один метод. В этотм случае имеет смысл задуматься о Фасаде.

//Отсюда - преимущества данного паттерна - Клиент изолирован полностью от компонентов сложной системы.

//Минус - в какой-то моменнт Фасад мождет стать АнтиПаттерном или выродится в  т.н. божественный объект в кот-м хранится слишком
//много данных, делает слишком много вещей и замыкают на себя слишком много ссылок. Это -плохая практика. И нарушается принцип СОЛИД.
//Божественные объекты слишком усложняют поддержку программы и дальнейшую разработку. Она становится оч тяжелорвесной и
// распутывать такие объекты при рефакторинге или принаписании новой логики, когда требуется лишь какя-то часть функционала.