// Компонет инпута
// - содержит 2 метода 	void LockPlayerInput(); и UnlockPlayerInput()

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InputHandlingComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PATTERNS_API UInputHandlingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	/** @brief Locks input from the player. Блокирует ввод от игрока.
	 * @details May be needed throutht cutscenes or animations. Может понадобиться в виде заставки или анимации.
	 */
	UFUNCTION(BlueprintCallable, Category= Input)
	void LockPlayerInput();

	/** @brief Unlock input from the player. РазБлокирует ввод от игрока.
	 */
	UFUNCTION(BlueprintCallable, Category= Input)
	void UnlockPlayerInput();
};
